// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MainThreadDispatcher+CullingMode
declare namespace UniRx.MainThreadDispatcher {
	enum CullingMode {
		Disabled = 0,
		Self = 1,
		All = 2,
	}
}
