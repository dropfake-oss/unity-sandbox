// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.EventPattern`2
declare namespace UniRx {
	class EventPattern<TSender, TEventArgs> extends System.Object implements UniRx.IEventPattern<TSender, TEventArgs> {
		readonly Sender: TSender
		readonly EventArgs: TEventArgs
		constructor(sender: TSender, e: TEventArgs)
		Equals(other: UniRx.EventPattern<TSender, TEventArgs>): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
	}
}
