// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactivePropertyExtensions
declare namespace UniRx {
	abstract class ReactivePropertyExtensions extends System.Object {
		static ToReactiveProperty<T>(source: System.IObservable<T>): UniRx.IReadOnlyReactiveProperty<T>
		static ToReactiveProperty<T>(source: System.IObservable<T>, initialValue: T): UniRx.IReadOnlyReactiveProperty<T>
		static ToReadOnlyReactiveProperty<T>(source: System.IObservable<T>): UniRx.ReadOnlyReactiveProperty<T>
		static WaitUntilValueChangedAsync<T>(source: UniRx.IReadOnlyReactiveProperty<T>, cancellationToken: any): any
		static GetAwaiter<T>(source: UniRx.IReadOnlyReactiveProperty<T>): any
		static ToSequentialReadOnlyReactiveProperty<T>(source: System.IObservable<T>): UniRx.ReadOnlyReactiveProperty<T>
		static ToReadOnlyReactiveProperty<T>(source: System.IObservable<T>, initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		static ToSequentialReadOnlyReactiveProperty<T>(source: System.IObservable<T>, initialValue: T): UniRx.ReadOnlyReactiveProperty<T>
		static SkipLatestValueOnSubscribe<T>(source: UniRx.IReadOnlyReactiveProperty<T>): System.IObservable<T>
		static CombineLatestValuesAreAllTrue(sources: any): System.IObservable<boolean>
		static CombineLatestValuesAreAllFalse(sources: any): System.IObservable<boolean>
	}
}
