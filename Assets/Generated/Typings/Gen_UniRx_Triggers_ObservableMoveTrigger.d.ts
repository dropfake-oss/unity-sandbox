// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableMoveTrigger
declare namespace UniRx.Triggers {
	class ObservableMoveTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnMoveAsObservable(): System.IObservable<any>
	}
}
