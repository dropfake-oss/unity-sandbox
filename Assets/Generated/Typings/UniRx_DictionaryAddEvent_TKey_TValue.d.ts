// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.DictionaryAddEvent`2
declare namespace UniRx {
	class DictionaryAddEvent<TKey, TValue> extends System.Object {
		readonly Key: TKey
		readonly Value: TValue
		constructor(key: TKey, value: TValue)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.DictionaryAddEvent<TKey, TValue>): boolean
	}
}
