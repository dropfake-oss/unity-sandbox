// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FVector4
declare namespace FixedPointMath {
	class FVector4 extends System.Object {
		x: FixedPointMath.Fixed
		y: FixedPointMath.Fixed
		z: FixedPointMath.Fixed
		w: FixedPointMath.Fixed
		static readonly zero: FixedPointMath.FVector4
		static readonly one: FixedPointMath.FVector4
		static readonly MinValue: FixedPointMath.FVector4
		static readonly MaxValue: FixedPointMath.FVector4
		readonly sqrMagnitude: FixedPointMath.Fixed
		readonly magnitude: FixedPointMath.Fixed
		readonly normalized: FixedPointMath.FVector4
		constructor(x: number, y: number, z: number, w: number)
		constructor(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed, w: FixedPointMath.Fixed)
		constructor(xyzw: FixedPointMath.Fixed)
		static Abs(other: FixedPointMath.FVector4): FixedPointMath.FVector4
		static ClampMagnitude(vector: FixedPointMath.FVector4, maxLength: FixedPointMath.Fixed): FixedPointMath.FVector4
		Scale(other: FixedPointMath.FVector4): void
		Set(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed): void
		static Lerp(from: FixedPointMath.FVector4, to: FixedPointMath.FVector4, percent: FixedPointMath.Fixed): FixedPointMath.FVector4
		ToString(): string
		Equals(obj: System.Object): boolean
		static Scale(vecA: FixedPointMath.FVector4, vecB: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Min(value1: FixedPointMath.FVector4, value2: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Min(value1: any, value2: any, result: any): void
		static Max(value1: FixedPointMath.FVector4, value2: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Distance(v1: FixedPointMath.FVector4, v2: FixedPointMath.FVector4): FixedPointMath.Fixed
		static Max(value1: any, value2: any, result: any): void
		MakeZero(): void
		IsZero(): boolean
		IsNearlyZero(): boolean
		static Dot(vector1: FixedPointMath.FVector4, vector2: FixedPointMath.FVector4): FixedPointMath.Fixed
		static Dot(vector1: any, vector2: any): FixedPointMath.Fixed
		static Add(value1: FixedPointMath.FVector4, value2: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Add(value1: any, value2: any, result: any): void
		static Divide(value1: FixedPointMath.FVector4, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FVector4
		static Divide(value1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static Subtract(value1: FixedPointMath.FVector4, value2: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Subtract(value1: any, value2: any, result: any): void
		GetHashCode(): number
		Negate(): void
		static Negate(value: FixedPointMath.FVector4): FixedPointMath.FVector4
		static Negate(value: any, result: any): void
		static Normalize(value: FixedPointMath.FVector4): FixedPointMath.FVector4
		Normalize(): void
		static Normalize(value: any, result: any): void
		static Multiply(value1: FixedPointMath.FVector4, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FVector4
		static Multiply(value1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static Angle(a: FixedPointMath.FVector4, b: FixedPointMath.FVector4): FixedPointMath.Fixed
		ToFVector3(): FixedPointMath.FVector3
	}
}
