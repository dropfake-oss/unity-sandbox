// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.EmptyObserver`1
declare namespace UniRx.InternalUtil {
	class EmptyObserver<T> extends System.Object implements System.IObserver<T> {
		static readonly Instance: UniRx.InternalUtil.EmptyObserver<T>
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
	}
}
