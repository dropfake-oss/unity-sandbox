// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IReactiveDictionary`2
declare namespace UniRx {
	interface IReactiveDictionary<TKey, TValue> extends UniRx.IReadOnlyReactiveDictionary<TKey, TValue> {
	}
}
