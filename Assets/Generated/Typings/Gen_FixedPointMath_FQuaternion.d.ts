// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FQuaternion
declare namespace FixedPointMath {
	class FQuaternion extends System.Object {
		x: FixedPointMath.Fixed
		y: FixedPointMath.Fixed
		z: FixedPointMath.Fixed
		w: FixedPointMath.Fixed
		static readonly identity: FixedPointMath.FQuaternion
		static readonly MaxValue: FixedPointMath.FQuaternion
		readonly eulerAngles: FixedPointMath.FVector3
		constructor(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed, w: FixedPointMath.Fixed)
		Set(new_x: FixedPointMath.Fixed, new_y: FixedPointMath.Fixed, new_z: FixedPointMath.Fixed, new_w: FixedPointMath.Fixed): void
		SetFromToRotation(fromDirection: FixedPointMath.FVector3, toDirection: FixedPointMath.FVector3): void
		static Angle(a: FixedPointMath.FQuaternion, b: FixedPointMath.FQuaternion): FixedPointMath.Fixed
		static Add(quaternion1: FixedPointMath.FQuaternion, quaternion2: FixedPointMath.FQuaternion): FixedPointMath.FQuaternion
		static LookRotation(forward: FixedPointMath.FVector3): FixedPointMath.FQuaternion
		static LookRotation(forward: FixedPointMath.FVector3, upwards: FixedPointMath.FVector3): FixedPointMath.FQuaternion
		static Slerp(from: FixedPointMath.FQuaternion, to: FixedPointMath.FQuaternion, t: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static RotateTowards(from: FixedPointMath.FQuaternion, to: FixedPointMath.FQuaternion, maxDegreesDelta: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static Euler(x: FixedPointMath.Fixed, y: FixedPointMath.Fixed, z: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static Euler(eulerAngles: FixedPointMath.FVector3): FixedPointMath.FQuaternion
		static AngleAxis(angle: FixedPointMath.Fixed, axis: FixedPointMath.FVector3): FixedPointMath.FQuaternion
		static CreateFromYawPitchRoll(yaw: FixedPointMath.Fixed, pitch: FixedPointMath.Fixed, roll: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static CreateFromYawPitchRoll(yaw: FixedPointMath.Fixed, pitch: FixedPointMath.Fixed, roll: FixedPointMath.Fixed, result: any): void
		static Add(quaternion1: any, quaternion2: any, result: any): void
		static Conjugate(value: FixedPointMath.FQuaternion): FixedPointMath.FQuaternion
		static Dot(a: FixedPointMath.FQuaternion, b: FixedPointMath.FQuaternion): FixedPointMath.Fixed
		static Inverse(rotation: FixedPointMath.FQuaternion): FixedPointMath.FQuaternion
		static FromToRotation(fromVector: FixedPointMath.FVector3, toVector: FixedPointMath.FVector3): FixedPointMath.FQuaternion
		static Lerp(a: FixedPointMath.FQuaternion, b: FixedPointMath.FQuaternion, t: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static LerpUnclamped(a: FixedPointMath.FQuaternion, b: FixedPointMath.FQuaternion, t: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static Subtract(quaternion1: FixedPointMath.FQuaternion, quaternion2: FixedPointMath.FQuaternion): FixedPointMath.FQuaternion
		static Subtract(quaternion1: any, quaternion2: any, result: any): void
		static Multiply(quaternion1: FixedPointMath.FQuaternion, quaternion2: FixedPointMath.FQuaternion): FixedPointMath.FQuaternion
		static Multiply(quaternion1: any, quaternion2: any, result: any): void
		static Multiply(quaternion1: FixedPointMath.FQuaternion, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FQuaternion
		static Multiply(quaternion1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		Normalize(): void
		static CreateFromMatrix(matrix: FixedPointMath.FMatrix): FixedPointMath.FQuaternion
		static CreateFromMatrix(matrix: any, result: any): void
		GetHashCode(): number
		Equals(obj: System.Object): boolean
		ToString(): string
	}
}
