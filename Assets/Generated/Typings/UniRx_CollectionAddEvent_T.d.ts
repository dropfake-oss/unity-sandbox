// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CollectionAddEvent`1
declare namespace UniRx {
	class CollectionAddEvent<T> extends System.Object {
		readonly Index: number
		readonly Value: T
		constructor(index: number, value: T)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.CollectionAddEvent<T>): boolean
	}
}
