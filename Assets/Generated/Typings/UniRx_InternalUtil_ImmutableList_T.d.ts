// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.ImmutableList`1
declare namespace UniRx.InternalUtil {
	class ImmutableList<T> extends System.Object {
		static readonly Empty: UniRx.InternalUtil.ImmutableList<T>
		readonly Data: T[]
		constructor(data: T[])
		Add(value: T): UniRx.InternalUtil.ImmutableList<T>
		Remove(value: T): UniRx.InternalUtil.ImmutableList<T>
		IndexOf(value: T): number
	}
}
