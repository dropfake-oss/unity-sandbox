// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.ObservableDebugExtensions
declare namespace UniRx.Diagnostics {
	abstract class ObservableDebugExtensions extends System.Object {
		static Debug<T>(source: System.IObservable<T>, label: string): System.IObservable<T>
		static Debug<T>(source: System.IObservable<T>, logger: UniRx.Diagnostics.Logger): System.IObservable<T>
	}
}
