// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FRandom
declare namespace FixedPointMath {
	class FRandom extends System.Object {
		static readonly MaxRandomInt: number
		static readonly OneOverMax: FixedPointMath.Fixed
		readonly Index: number
		static New(seed: number): FixedPointMath.FRandom
		Next(): number
		NextF64(): FixedPointMath.Fixed
		Next(minValue: number, maxValue: number): number
		Next(minValue: number, maxValue: number): FixedPointMath.Fixed
	}
}
