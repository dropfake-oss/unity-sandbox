// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IScheduler
declare namespace UniRx {
	interface IScheduler {
		readonly Now: any
		Schedule(action: (() => void)): System.IDisposable
		Schedule(dueTime: any, action: (() => void)): System.IDisposable
		Schedule(dueTime: any, action: (() => void)): System.IDisposable
		Schedule(action: ((obj: (() => void)) => void)): System.IDisposable
		Schedule(dueTime: any, action: ((obj: ((obj: any) => void)) => void)): System.IDisposable
		Schedule(dueTime: any, action: ((obj: ((obj: any) => void)) => void)): System.IDisposable
	}
}
