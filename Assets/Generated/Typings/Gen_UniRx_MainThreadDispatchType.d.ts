// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MainThreadDispatchType
declare namespace UniRx {
	enum MainThreadDispatchType {
		Update = 0,
		FixedUpdate = 1,
		EndOfFrame = 2,
		GameObjectUpdate = 3,
		LateUpdate = 4,
	}
}
