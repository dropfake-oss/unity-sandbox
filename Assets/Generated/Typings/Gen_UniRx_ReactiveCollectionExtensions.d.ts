// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveCollectionExtensions
declare namespace UniRx {
	abstract class ReactiveCollectionExtensions extends System.Object {
		static ToReactiveCollection<T>(source: any): UniRx.ReactiveCollection<T>
	}
}
