// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.NotificationKind
declare namespace UniRx {
	enum NotificationKind {
		OnNext = 0,
		OnError = 1,
		OnCompleted = 2,
	}
}
