// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReadOnlyReactiveProperty`1
declare namespace UniRx {
	class ReadOnlyReactiveProperty<T> extends System.Object implements System.IObservable<T>, System.IObserver<T>, UniRx.IReadOnlyReactiveProperty<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		readonly Value: T
		readonly HasValue: boolean
		constructor(source: System.IObservable<T>)
		constructor(source: System.IObservable<T>, distinctUntilChanged: boolean)
		constructor(source: System.IObservable<T>, initialValue: T)
		constructor(source: System.IObservable<T>, initialValue: T, distinctUntilChanged: boolean)
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Dispose(): void
		ToString(): string
		IsRequiredSubscribeOnCurrentThread(): boolean
	}
}
