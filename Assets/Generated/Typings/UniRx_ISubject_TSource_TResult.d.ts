// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ISubject`2
declare namespace UniRx {
	interface ISubject<TSource, TResult> extends System.IObservable<TResult>, System.IObserver<TSource> {
		Synchronize(): System.IObserver<TSource>
		Synchronize(gate: System.Object): System.IObserver<TSource>
	}
}
