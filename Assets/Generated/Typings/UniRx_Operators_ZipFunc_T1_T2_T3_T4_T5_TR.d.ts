// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Operators.ZipFunc`6
declare namespace UniRx.Operators {
	class ZipFunc<T1, T2, T3, T4, T5, TR> extends System.Object {
		constructor(object: System.Object, method: any)
		Invoke(arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5): TR
		BeginInvoke(arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5, callback: ((ar: any) => void), object: System.Object): any
		EndInvoke(result: any): TR
	}
}
