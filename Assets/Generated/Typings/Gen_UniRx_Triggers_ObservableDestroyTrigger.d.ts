// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableDestroyTrigger
declare namespace UniRx.Triggers {
	class ObservableDestroyTrigger extends UnityEngine.Object {
		readonly IsActivated: boolean
		readonly IsCalledOnDestroy: boolean
		constructor()
		OnDestroyAsObservable(): System.IObservable<UniRx.Unit>
		ForceRaiseOnDestroy(): void
		AddDisposableOnDestroy(disposable: System.IDisposable): void
	}
}
