// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CoroutineAsyncBridge
declare namespace UniRx {
	class CoroutineAsyncBridge extends System.Object {
		readonly IsCompleted: boolean
		static Start<T>(awaitTarget: T): UniRx.CoroutineAsyncBridge
		OnCompleted(continuation: (() => void)): void
		GetResult(): void
	}
}
