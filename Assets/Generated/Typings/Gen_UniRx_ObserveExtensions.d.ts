// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ObserveExtensions
declare namespace UniRx {
	abstract class ObserveExtensions extends System.Object {
		static ObserveEveryValueChanged<TSource, TProperty>(source: TSource, propertySelector: ((arg: TSource) => TProperty), frameCountType: UniRx.FrameCountType, fastDestroyCheck: boolean): System.IObservable<TProperty>
		static ObserveEveryValueChanged<TSource, TProperty>(source: TSource, propertySelector: ((arg: TSource) => TProperty), frameCountType: UniRx.FrameCountType, comparer: any): System.IObservable<TProperty>
		static ObserveEveryValueChanged<TSource, TProperty>(source: TSource, propertySelector: ((arg: TSource) => TProperty), frameCountType: UniRx.FrameCountType, comparer: any, fastDestroyCheck: boolean): System.IObservable<TProperty>
	}
}
