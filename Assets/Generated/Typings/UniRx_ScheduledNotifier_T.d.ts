// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ScheduledNotifier`1
declare namespace UniRx {
	class ScheduledNotifier<T> extends System.Object implements System.IObservable<T> {
		constructor()
		constructor(scheduler: UniRx.IScheduler)
		Report(value: T): void
		Report(value: T, dueTime: any): System.IDisposable
		Report(value: T, dueTime: any): System.IDisposable
		Subscribe(observer: System.IObserver<T>): System.IDisposable
	}
}
