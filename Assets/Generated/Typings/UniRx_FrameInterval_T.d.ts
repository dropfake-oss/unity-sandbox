// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.FrameInterval`1
declare namespace UniRx {
	class FrameInterval<T> extends System.Object {
		readonly Value: T
		readonly Interval: number
		constructor(value: T, interval: number)
		Equals(other: UniRx.FrameInterval<T>): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		ToString(): string
	}
}
