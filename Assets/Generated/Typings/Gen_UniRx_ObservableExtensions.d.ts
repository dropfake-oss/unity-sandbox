// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ObservableExtensions
declare namespace UniRx {
	abstract class ObservableExtensions extends System.Object {
		static Subscribe<T>(source: System.IObservable<T>): System.IDisposable
		static Subscribe<T>(source: System.IObservable<T>, onNext: ((obj: T) => void)): System.IDisposable
		static Subscribe<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		static Subscribe<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onCompleted: (() => void)): System.IDisposable
		static Subscribe<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		static SubscribeWithState<T, TState>(source: System.IObservable<T>, state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		static SubscribeWithState<T, TState>(source: System.IObservable<T>, state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		static SubscribeWithState<T, TState>(source: System.IObservable<T>, state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		static SubscribeWithState<T, TState>(source: System.IObservable<T>, state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		static SubscribeWithState2<T, TState1, TState2>(source: System.IObservable<T>, state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		static SubscribeWithState2<T, TState1, TState2>(source: System.IObservable<T>, state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		static SubscribeWithState2<T, TState1, TState2>(source: System.IObservable<T>, state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		static SubscribeWithState2<T, TState1, TState2>(source: System.IObservable<T>, state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		static SubscribeWithState3<T, TState1, TState2, TState3>(source: System.IObservable<T>, state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		static SubscribeWithState3<T, TState1, TState2, TState3>(source: System.IObservable<T>, state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		static SubscribeWithState3<T, TState1, TState2, TState3>(source: System.IObservable<T>, state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		static SubscribeWithState3<T, TState1, TState2, TState3>(source: System.IObservable<T>, state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
	}
}
