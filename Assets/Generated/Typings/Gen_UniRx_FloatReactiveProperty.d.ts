// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.FloatReactiveProperty
declare namespace UniRx {
	class FloatReactiveProperty extends UniRx.ReactiveProperty<number> implements System.IObservable<number>, UniRx.IReadOnlyReactiveProperty<number>, UniRx.IReactiveProperty<number>, UniRx.IOptimizedObservable<number>, System.IDisposable {
		constructor()
		constructor(initialValue: number)
	}
}
