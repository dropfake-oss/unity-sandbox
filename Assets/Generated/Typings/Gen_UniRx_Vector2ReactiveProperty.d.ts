// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Vector2ReactiveProperty
declare namespace UniRx {
	class Vector2ReactiveProperty extends UniRx.ReactiveProperty<UnityEngine.Vector2> implements System.IObservable<UnityEngine.Vector2>, UniRx.IReadOnlyReactiveProperty<UnityEngine.Vector2>, UniRx.IReactiveProperty<UnityEngine.Vector2>, UniRx.IOptimizedObservable<UnityEngine.Vector2>, System.IDisposable {
		constructor()
		constructor(initialValue: UnityEngine.Vector2)
	}
}
