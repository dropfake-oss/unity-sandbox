// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CancellationDisposable
declare namespace UniRx {
	class CancellationDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly Token: any
		readonly IsDisposed: boolean
		constructor(cts: any)
		constructor()
		Dispose(): void
	}
}
