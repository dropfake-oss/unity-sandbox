// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.TimeInterval`1
declare namespace UniRx {
	class TimeInterval<T> extends System.Object {
		readonly Value: T
		readonly Interval: any
		constructor(value: T, interval: any)
		Equals(other: UniRx.TimeInterval<T>): boolean
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		ToString(): string
	}
}
