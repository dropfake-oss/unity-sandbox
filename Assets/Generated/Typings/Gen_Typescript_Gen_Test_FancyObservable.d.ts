// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Typescript.Gen.Test.FancyObservable
declare namespace Typescript.Gen.Test {
	class FancyObservable extends System.Object implements System.IObservable<[ System.Collections.Generic.List<string>, string, string ]> {
		constructor()
		Subscribe(observer: System.IObserver<[ System.Collections.Generic.List<string>, string, string ]>): System.IDisposable
		Update(entities: System.Collections.Generic.List<string>, cmd: string, data: string): void
		Commit(): void
		Scan(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Scan(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		GetAwaiter(): UniRx.AsyncSubject<TSource>
		GetAwaiter(cancellationToken: any): UniRx.AsyncSubject<TSource>
		Multicast(subject: UniRx.ISubject<[ System.Collections.Generic.List<string>, string, string ]>): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Publish(): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Publish(initialValue: T): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		PublishLast(): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(bufferSize: number): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(window: any): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Replay(bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Share(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Wait(): T
		Wait(timeout: any): T
		Concat(): System.IObservable<TSource>
		Concat(...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		Merge(...seconds: System.IObservable<T>[]): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Merge(second: System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Merge(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Merge(maxConcurrent: number): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Zip(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		CombineLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		ZipLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		Switch(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		WithLatestFrom(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		StartWith(value: T): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(valueFactory: (() => any)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(...values: T[]): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(values: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(scheduler: UniRx.IScheduler, value: T): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(scheduler: UniRx.IScheduler, values: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		StartWith(scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Synchronize(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Synchronize(gate: System.Object): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ObserveOn(scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SubscribeOn(scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DelaySubscription(dueTime: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DelaySubscription(dueTime: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Amb(second: System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		AsObservable(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Cast(): System.IObservable<TResult>
		Cast(witness: TResult): System.IObservable<TResult>
		OfType(): System.IObservable<TResult>
		OfType(witness: TResult): System.IObservable<TResult>
		AsUnitObservable(): System.IObservable<UniRx.Unit>
		AsSingleUnitObservable(): System.IObservable<UniRx.Unit>
		Repeat(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		RepeatSafe(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Select(selector: ((arg: T) => TR)): System.IObservable<TR>
		Select(selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		Where(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Where(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ContinueWith(other: System.IObservable<TR>): System.IObservable<TR>
		ContinueWith(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(other: System.IObservable<TR>): System.IObservable<TR>
		SelectMany(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		SelectMany(selector: ((arg: TSource) => any)): System.IObservable<TResult>
		SelectMany(selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		ToArray(): System.IObservable<T[]>
		ToList(): System.IObservable<any>
		Do(observer: System.IObserver<[ System.Collections.Generic.List<string>, string, string ]>): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Do(onNext: ((obj: any) => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Do(onNext: ((obj: any) => void), onError: ((obj: System.Exception) => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Do(onNext: ((obj: any) => void), onCompleted: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Do(onNext: ((obj: any) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DoOnError(onError: ((obj: System.Exception) => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DoOnCompleted(onCompleted: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DoOnTerminate(onTerminate: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DoOnSubscribe(onSubscribe: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DoOnCancel(onCancel: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Materialize(): System.IObservable<UniRx.Notification<T>>
		Dematerialize(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DefaultIfEmpty(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DefaultIfEmpty(defaultValue: T): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Distinct(): System.IObservable<TSource>
		Distinct(comparer: any): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		DistinctUntilChanged(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DistinctUntilChanged(comparer: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		IgnoreElements(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ForEachAsync(onNext: ((obj: any) => void)): System.IObservable<UniRx.Unit>
		ForEachAsync(onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		Finally(finallyAction: (() => void)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Catch(errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		CatchIgnore(): System.IObservable<TSource>
		CatchIgnore(errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		Retry(): System.IObservable<TSource>
		Retry(retryCount: number): System.IObservable<TSource>
		OnErrorRetry(): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void)): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		Take(count: number): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Take(duration: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Take(duration: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeWhile(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeUntil(other: System.IObservable<TOther>): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeLast(count: number): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeLast(duration: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeLast(duration: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Skip(count: number): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Skip(duration: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Skip(duration: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SkipWhile(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SkipWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SkipUntil(other: System.IObservable<TOther>): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Buffer(count: number): System.IObservable<any>
		Buffer(count: number, skip: number): System.IObservable<any>
		Buffer(timeSpan: any): System.IObservable<any>
		Buffer(timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, count: number): System.IObservable<any>
		Buffer(timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		Pairwise(): System.IObservable<UniRx.Pair<T>>
		Pairwise(selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		Last(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Last(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		LastOrDefault(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		LastOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		First(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		First(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		FirstOrDefault(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		FirstOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Single(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Single(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SingleOrDefault(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SingleOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		GroupBy(keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		Timestamp(): System.IObservable<UniRx.Timestamped<TSource>>
		Timestamp(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		TimeInterval(): System.IObservable<UniRx.TimeInterval<TSource>>
		TimeInterval(scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		Delay(dueTime: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Delay(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Sample(interval: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Sample(interval: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Throttle(dueTime: any): System.IObservable<TSource>
		Throttle(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		ThrottleFirst(dueTime: any): System.IObservable<TSource>
		ThrottleFirst(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Timeout(dueTime: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Timeout(dueTime: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SelectMany(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		DelayFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Sample(sampler: System.IObservable<T2>): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SampleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ThrottleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		ThrottleFirstFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		TimeoutFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		DelayFrameSubscription(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ToYieldInstruction(): UniRx.ObservableYieldInstruction<[ System.Collections.Generic.List<string>, string, string ]>
		ToYieldInstruction(cancel: any): UniRx.ObservableYieldInstruction<[ System.Collections.Generic.List<string>, string, string ]>
		ToYieldInstruction(throwOnError: boolean): UniRx.ObservableYieldInstruction<[ System.Collections.Generic.List<string>, string, string ]>
		ToYieldInstruction(throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<[ System.Collections.Generic.List<string>, string, string ]>
		ToAwaitableEnumerator(cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: any) => void), cancel: any): any
		ToAwaitableEnumerator(onError: ((obj: System.Exception) => void), cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: any) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(cancel: any): any
		StartAsCoroutine(onResult: ((obj: any) => void), cancel: any): any
		StartAsCoroutine(onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(onResult: ((obj: any) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		ObserveOnMainThread(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		ObserveOnMainThread(dispatchType: UniRx.MainThreadDispatchType): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		SubscribeOnMainThread(): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeUntilDestroy(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeUntilDestroy(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeUntilDisable(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		TakeUntilDisable(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		RepeatUntilDestroy(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		RepeatUntilDestroy(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		RepeatUntilDisable(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		RepeatUntilDisable(target: any): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		FrameInterval(): System.IObservable<UniRx.FrameInterval<T>>
		FrameTimeInterval(ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		BatchFrame(): System.IObservable<any>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		BatchFrame(): System.IObservable<UniRx.Unit>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		Subscribe(): System.IDisposable
		Subscribe(onNext: ((obj: any) => void)): System.IDisposable
		Subscribe(onNext: ((obj: any) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		Subscribe(onNext: ((obj: any) => void), onCompleted: (() => void)): System.IDisposable
		Subscribe(onNext: ((obj: any) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		IsRequiredSubscribeOnCurrentThread(): boolean
		IsRequiredSubscribeOnCurrentThread(scheduler: UniRx.IScheduler): boolean
		ToTask(): any
		ToTask(state: System.Object): any
		ToTask(cancellationToken: any): any
		ToTask(cancellationToken: any, state: System.Object): any
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand<[ System.Collections.Generic.List<string>, string, string ]>
		BindToButtonOnClick(button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
		ToReactiveProperty(): UniRx.IReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		ToReactiveProperty(initialValue: T): UniRx.IReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		ToReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		ToSequentialReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		ToReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		ToSequentialReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<[ System.Collections.Generic.List<string>, string, string ]>
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any, selector: ((arg: T) => string)): System.IDisposable
		SubscribeToInteractable(selectable: any): System.IDisposable
		LogToUnityDebug(): System.IDisposable
		Debug(label: string): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
		Debug(logger: UniRx.Diagnostics.Logger): System.IObservable<[ System.Collections.Generic.List<string>, string, string ]>
	}
}
