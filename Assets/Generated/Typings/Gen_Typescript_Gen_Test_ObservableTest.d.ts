// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Typescript.Gen.Test.ObservableTest
declare namespace Typescript.Gen.Test {
	class ObservableTest extends System.Object {
		constructor()
		SimpleObservableTest(): void
		FancyObservableTest(): void
		GetData(): System.Object
	}
}
