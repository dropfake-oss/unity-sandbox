// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.Array
declare namespace System {
	abstract class Array extends System.Object {
		readonly LongLength: number
		readonly IsFixedSize: boolean
		readonly IsReadOnly: boolean
		readonly IsSynchronized: boolean
		readonly SyncRoot: System.Object
		readonly Length: number
		readonly Rank: number
		static CreateInstance(elementType: any, ...lengths: number[]): System.Array
		static AsReadOnly<T>(array: T[]): any
		static Resize<T>(array: any, newSize: number): void
		CopyTo(array: System.Array, index: number): void
		Clone(): System.Object
		static BinarySearch(array: System.Array, value: System.Object): number
		static ConvertAll<TInput, TOutput>(array: TInput[], converter: ((input: TInput) => TOutput)): TOutput[]
		static Copy(sourceArray: System.Array, destinationArray: System.Array, length: number): void
		static Copy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		CopyTo(array: System.Array, index: number): void
		static ForEach<T>(array: T[], action: ((obj: T) => void)): void
		GetLongLength(dimension: number): number
		GetValue(index: number): System.Object
		GetValue(index1: number, index2: number): System.Object
		GetValue(index1: number, index2: number, index3: number): System.Object
		GetValue(...indices: number[]): System.Object
		static BinarySearch(array: System.Array, index: number, length: number, value: System.Object): number
		static BinarySearch(array: System.Array, value: System.Object, comparer: any): number
		static BinarySearch(array: System.Array, index: number, length: number, value: System.Object, comparer: any): number
		static BinarySearch<T>(array: T[], value: T): number
		static BinarySearch<T>(array: T[], value: T, comparer: any): number
		static BinarySearch<T>(array: T[], index: number, length: number, value: T): number
		static BinarySearch<T>(array: T[], index: number, length: number, value: T, comparer: any): number
		static IndexOf(array: System.Array, value: System.Object): number
		static IndexOf(array: System.Array, value: System.Object, startIndex: number): number
		static IndexOf(array: System.Array, value: System.Object, startIndex: number, count: number): number
		static IndexOf<T>(array: T[], value: T): number
		static IndexOf<T>(array: T[], value: T, startIndex: number): number
		static IndexOf<T>(array: T[], value: T, startIndex: number, count: number): number
		static LastIndexOf(array: System.Array, value: System.Object): number
		static LastIndexOf(array: System.Array, value: System.Object, startIndex: number): number
		static LastIndexOf(array: System.Array, value: System.Object, startIndex: number, count: number): number
		static LastIndexOf<T>(array: T[], value: T): number
		static LastIndexOf<T>(array: T[], value: T, startIndex: number): number
		static LastIndexOf<T>(array: T[], value: T, startIndex: number, count: number): number
		static Reverse(array: System.Array): void
		static Reverse(array: System.Array, index: number, length: number): void
		static Reverse<T>(array: T[]): void
		static Reverse<T>(array: T[], index: number, length: number): void
		SetValue(value: System.Object, index: number): void
		SetValue(value: System.Object, index1: number, index2: number): void
		SetValue(value: System.Object, index1: number, index2: number, index3: number): void
		SetValue(value: System.Object, ...indices: number[]): void
		static Sort(array: System.Array): void
		static Sort(array: System.Array, index: number, length: number): void
		static Sort(array: System.Array, comparer: any): void
		static Sort(array: System.Array, index: number, length: number, comparer: any): void
		static Sort(keys: System.Array, items: System.Array): void
		static Sort(keys: System.Array, items: System.Array, comparer: any): void
		static Sort(keys: System.Array, items: System.Array, index: number, length: number): void
		static Sort(keys: System.Array, items: System.Array, index: number, length: number, comparer: any): void
		static Sort<T>(array: T[]): void
		static Sort<T>(array: T[], index: number, length: number): void
		static Sort<T>(array: T[], comparer: any): void
		static Sort<T>(array: T[], index: number, length: number, comparer: any): void
		static Sort<T>(array: T[], comparison: ((x: T, y: T) => number)): void
		static Sort<TKey, TValue>(keys: TKey[], items: TValue[]): void
		static Sort<TKey, TValue>(keys: TKey[], items: TValue[], index: number, length: number): void
		static Sort<TKey, TValue>(keys: TKey[], items: TValue[], comparer: any): void
		static Sort<TKey, TValue>(keys: TKey[], items: TValue[], index: number, length: number, comparer: any): void
		static Exists<T>(array: T[], match: ((obj: T) => boolean)): boolean
		static Fill<T>(array: T[], value: T): void
		static Fill<T>(array: T[], value: T, startIndex: number, count: number): void
		static Find<T>(array: T[], match: ((obj: T) => boolean)): T
		static FindAll<T>(array: T[], match: ((obj: T) => boolean)): T[]
		static FindIndex<T>(array: T[], match: ((obj: T) => boolean)): number
		static FindIndex<T>(array: T[], startIndex: number, match: ((obj: T) => boolean)): number
		static FindIndex<T>(array: T[], startIndex: number, count: number, match: ((obj: T) => boolean)): number
		static FindLast<T>(array: T[], match: ((obj: T) => boolean)): T
		static FindLastIndex<T>(array: T[], match: ((obj: T) => boolean)): number
		static FindLastIndex<T>(array: T[], startIndex: number, match: ((obj: T) => boolean)): number
		static FindLastIndex<T>(array: T[], startIndex: number, count: number, match: ((obj: T) => boolean)): number
		static TrueForAll<T>(array: T[], match: ((obj: T) => boolean)): boolean
		GetEnumerator(): any
		GetLength(dimension: number): number
		GetLowerBound(dimension: number): number
		GetValue(...indices: number[]): System.Object
		SetValue(value: System.Object, ...indices: number[]): void
		GetUpperBound(dimension: number): number
		GetValue(index: number): System.Object
		GetValue(index1: number, index2: number): System.Object
		GetValue(index1: number, index2: number, index3: number): System.Object
		SetValue(value: System.Object, index: number): void
		SetValue(value: System.Object, index1: number, index2: number): void
		SetValue(value: System.Object, index1: number, index2: number, index3: number): void
		static CreateInstance(elementType: any, length: number): System.Array
		static CreateInstance(elementType: any, length1: number, length2: number): System.Array
		static CreateInstance(elementType: any, length1: number, length2: number, length3: number): System.Array
		static CreateInstance(elementType: any, ...lengths: number[]): System.Array
		static CreateInstance(elementType: any, lengths: number[], lowerBounds: number[]): System.Array
		static Clear(array: System.Array, index: number, length: number): void
		static Copy(sourceArray: System.Array, destinationArray: System.Array, length: number): void
		static Copy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		static ConstrainedCopy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		static Empty<T>(): T[]
		Initialize(): void
	}
}
