// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ScenePlaybackDetector
declare namespace UniRx {
	class ScenePlaybackDetector extends System.Object {
		static IsPlaying: boolean
		constructor()
		static OnDidReloadScripts(): void
	}
}
