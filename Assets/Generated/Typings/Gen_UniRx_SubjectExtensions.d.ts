// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.SubjectExtensions
declare namespace UniRx {
	abstract class SubjectExtensions extends System.Object {
		static Synchronize<T>(subject: UniRx.ISubject<T>): UniRx.ISubject<T>
		static Synchronize<T>(subject: UniRx.ISubject<T>, gate: System.Object): UniRx.ISubject<T>
	}
}
