// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.IObserver`1
declare namespace System {
	interface IObserver<T> {
		OnNext(value: T): void
		OnError(error: System.Exception): void
		OnCompleted(): void
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
	}
}
