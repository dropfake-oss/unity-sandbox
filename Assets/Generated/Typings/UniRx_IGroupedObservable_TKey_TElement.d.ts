// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IGroupedObservable`2
declare namespace UniRx {
	interface IGroupedObservable<TKey, TElement> extends System.IObservable<TElement> {
		readonly Key: TKey
		Scan(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Scan(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		GetAwaiter(): UniRx.AsyncSubject<TSource>
		GetAwaiter(cancellationToken: any): UniRx.AsyncSubject<TSource>
		Multicast(subject: UniRx.ISubject<TElement>): UniRx.IConnectableObservable<TElement>
		Publish(): UniRx.IConnectableObservable<TElement>
		Publish(initialValue: T): UniRx.IConnectableObservable<TElement>
		PublishLast(): UniRx.IConnectableObservable<TElement>
		Replay(): UniRx.IConnectableObservable<TElement>
		Replay(scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<TElement>
		Replay(bufferSize: number): UniRx.IConnectableObservable<TElement>
		Replay(bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<TElement>
		Replay(window: any): UniRx.IConnectableObservable<TElement>
		Replay(window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<TElement>
		Replay(bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<TElement>
		Share(): System.IObservable<TElement>
		Wait(): T
		Wait(timeout: any): T
		Concat(): System.IObservable<TSource>
		Concat(...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		Merge(...seconds: System.IObservable<T>[]): System.IObservable<TElement>
		Merge(second: System.IObservable<TElement>, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		Merge(): System.IObservable<TElement>
		Merge(maxConcurrent: number): System.IObservable<TElement>
		Zip(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		CombineLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		ZipLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		Switch(): System.IObservable<TElement>
		WithLatestFrom(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		StartWith(value: T): System.IObservable<TElement>
		StartWith(valueFactory: (() => TElement)): System.IObservable<TElement>
		StartWith(...values: T[]): System.IObservable<TElement>
		StartWith(values: any): System.IObservable<TElement>
		StartWith(scheduler: UniRx.IScheduler, value: T): System.IObservable<TElement>
		StartWith(scheduler: UniRx.IScheduler, values: any): System.IObservable<TElement>
		StartWith(scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<TElement>
		Synchronize(): System.IObservable<TElement>
		Synchronize(gate: System.Object): System.IObservable<TElement>
		ObserveOn(scheduler: UniRx.IScheduler): System.IObservable<TElement>
		SubscribeOn(scheduler: UniRx.IScheduler): System.IObservable<TElement>
		DelaySubscription(dueTime: any): System.IObservable<TElement>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		DelaySubscription(dueTime: any): System.IObservable<TElement>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		Amb(second: System.IObservable<TElement>): System.IObservable<TElement>
		AsObservable(): System.IObservable<TElement>
		Cast(): System.IObservable<TResult>
		Cast(witness: TResult): System.IObservable<TResult>
		OfType(): System.IObservable<TResult>
		OfType(witness: TResult): System.IObservable<TResult>
		AsUnitObservable(): System.IObservable<UniRx.Unit>
		AsSingleUnitObservable(): System.IObservable<UniRx.Unit>
		Repeat(): System.IObservable<TElement>
		RepeatSafe(): System.IObservable<TElement>
		Select(selector: ((arg: T) => TR)): System.IObservable<TR>
		Select(selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		Where(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		Where(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<TElement>
		ContinueWith(other: System.IObservable<TR>): System.IObservable<TR>
		ContinueWith(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(other: System.IObservable<TR>): System.IObservable<TR>
		SelectMany(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		SelectMany(selector: ((arg: TSource) => any)): System.IObservable<TResult>
		SelectMany(selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		ToArray(): System.IObservable<T[]>
		ToList(): System.IObservable<any>
		Do(observer: System.IObserver<TElement>): System.IObservable<TElement>
		Do(onNext: ((obj: TElement) => void)): System.IObservable<TElement>
		Do(onNext: ((obj: TElement) => void), onError: ((obj: System.Exception) => void)): System.IObservable<TElement>
		Do(onNext: ((obj: TElement) => void), onCompleted: (() => void)): System.IObservable<TElement>
		Do(onNext: ((obj: TElement) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<TElement>
		DoOnError(onError: ((obj: System.Exception) => void)): System.IObservable<TElement>
		DoOnCompleted(onCompleted: (() => void)): System.IObservable<TElement>
		DoOnTerminate(onTerminate: (() => void)): System.IObservable<TElement>
		DoOnSubscribe(onSubscribe: (() => void)): System.IObservable<TElement>
		DoOnCancel(onCancel: (() => void)): System.IObservable<TElement>
		Materialize(): System.IObservable<UniRx.Notification<T>>
		Dematerialize(): System.IObservable<TElement>
		DefaultIfEmpty(): System.IObservable<TElement>
		DefaultIfEmpty(defaultValue: T): System.IObservable<TElement>
		Distinct(): System.IObservable<TSource>
		Distinct(comparer: any): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		DistinctUntilChanged(): System.IObservable<TElement>
		DistinctUntilChanged(comparer: any): System.IObservable<TElement>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey)): System.IObservable<TElement>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<TElement>
		IgnoreElements(): System.IObservable<TElement>
		ForEachAsync(onNext: ((obj: TElement) => void)): System.IObservable<UniRx.Unit>
		ForEachAsync(onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		Finally(finallyAction: (() => void)): System.IObservable<TElement>
		Catch(errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<TElement>
		CatchIgnore(): System.IObservable<TSource>
		CatchIgnore(errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		Retry(): System.IObservable<TSource>
		Retry(retryCount: number): System.IObservable<TSource>
		OnErrorRetry(): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void)): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		Take(count: number): System.IObservable<TElement>
		Take(duration: any): System.IObservable<TElement>
		Take(duration: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		TakeWhile(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		TakeWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<TElement>
		TakeUntil(other: System.IObservable<TOther>): System.IObservable<TElement>
		TakeLast(count: number): System.IObservable<TElement>
		TakeLast(duration: any): System.IObservable<TElement>
		TakeLast(duration: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		Skip(count: number): System.IObservable<TElement>
		Skip(duration: any): System.IObservable<TElement>
		Skip(duration: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		SkipWhile(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		SkipWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<TElement>
		SkipUntil(other: System.IObservable<TOther>): System.IObservable<TElement>
		Buffer(count: number): System.IObservable<any>
		Buffer(count: number, skip: number): System.IObservable<any>
		Buffer(timeSpan: any): System.IObservable<any>
		Buffer(timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, count: number): System.IObservable<any>
		Buffer(timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		Pairwise(): System.IObservable<UniRx.Pair<T>>
		Pairwise(selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		Last(): System.IObservable<TElement>
		Last(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		LastOrDefault(): System.IObservable<TElement>
		LastOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		First(): System.IObservable<TElement>
		First(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		FirstOrDefault(): System.IObservable<TElement>
		FirstOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		Single(): System.IObservable<TElement>
		Single(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		SingleOrDefault(): System.IObservable<TElement>
		SingleOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<TElement>
		GroupBy(keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		Timestamp(): System.IObservable<UniRx.Timestamped<TSource>>
		Timestamp(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		TimeInterval(): System.IObservable<UniRx.TimeInterval<TSource>>
		TimeInterval(scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		Delay(dueTime: any): System.IObservable<TElement>
		Delay(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Sample(interval: any): System.IObservable<TElement>
		Sample(interval: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		Throttle(dueTime: any): System.IObservable<TSource>
		Throttle(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		ThrottleFirst(dueTime: any): System.IObservable<TSource>
		ThrottleFirst(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Timeout(dueTime: any): System.IObservable<TElement>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		Timeout(dueTime: any): System.IObservable<TElement>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TElement>
		SelectMany(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		DelayFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TElement>
		Sample(sampler: System.IObservable<T2>): System.IObservable<TElement>
		SampleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TElement>
		ThrottleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		ThrottleFirstFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		TimeoutFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TElement>
		DelayFrameSubscription(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TElement>
		ToYieldInstruction(): UniRx.ObservableYieldInstruction<TElement>
		ToYieldInstruction(cancel: any): UniRx.ObservableYieldInstruction<TElement>
		ToYieldInstruction(throwOnError: boolean): UniRx.ObservableYieldInstruction<TElement>
		ToYieldInstruction(throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<TElement>
		ToAwaitableEnumerator(cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: TElement) => void), cancel: any): any
		ToAwaitableEnumerator(onError: ((obj: System.Exception) => void), cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: TElement) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(cancel: any): any
		StartAsCoroutine(onResult: ((obj: TElement) => void), cancel: any): any
		StartAsCoroutine(onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(onResult: ((obj: TElement) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		ObserveOnMainThread(): System.IObservable<TElement>
		ObserveOnMainThread(dispatchType: UniRx.MainThreadDispatchType): System.IObservable<TElement>
		SubscribeOnMainThread(): System.IObservable<TElement>
		TakeUntilDestroy(target: any): System.IObservable<TElement>
		TakeUntilDestroy(target: any): System.IObservable<TElement>
		TakeUntilDisable(target: any): System.IObservable<TElement>
		TakeUntilDisable(target: any): System.IObservable<TElement>
		RepeatUntilDestroy(target: any): System.IObservable<TElement>
		RepeatUntilDestroy(target: any): System.IObservable<TElement>
		RepeatUntilDisable(target: any): System.IObservable<TElement>
		RepeatUntilDisable(target: any): System.IObservable<TElement>
		FrameInterval(): System.IObservable<UniRx.FrameInterval<T>>
		FrameTimeInterval(ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		BatchFrame(): System.IObservable<any>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		BatchFrame(): System.IObservable<UniRx.Unit>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		Subscribe(): System.IDisposable
		Subscribe(onNext: ((obj: TElement) => void)): System.IDisposable
		Subscribe(onNext: ((obj: TElement) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		Subscribe(onNext: ((obj: TElement) => void), onCompleted: (() => void)): System.IDisposable
		Subscribe(onNext: ((obj: TElement) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		IsRequiredSubscribeOnCurrentThread(): boolean
		IsRequiredSubscribeOnCurrentThread(scheduler: UniRx.IScheduler): boolean
		ToTask(): any
		ToTask(state: System.Object): any
		ToTask(cancellationToken: any): any
		ToTask(cancellationToken: any, state: System.Object): any
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand<TElement>
		BindToButtonOnClick(button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
		ToReactiveProperty(): UniRx.IReadOnlyReactiveProperty<TElement>
		ToReactiveProperty(initialValue: T): UniRx.IReadOnlyReactiveProperty<TElement>
		ToReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<TElement>
		ToSequentialReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<TElement>
		ToReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<TElement>
		ToSequentialReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<TElement>
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any, selector: ((arg: T) => string)): System.IDisposable
		SubscribeToInteractable(selectable: any): System.IDisposable
		LogToUnityDebug(): System.IDisposable
		Debug(label: string): System.IObservable<TElement>
		Debug(logger: UniRx.Diagnostics.Logger): System.IObservable<TElement>
	}
}
