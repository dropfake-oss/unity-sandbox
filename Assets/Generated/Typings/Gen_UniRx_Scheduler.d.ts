// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Scheduler
declare namespace UniRx {
	abstract class Scheduler extends System.Object {
		static readonly CurrentThread: UniRx.IScheduler
		static readonly Immediate: UniRx.IScheduler
		static readonly ThreadPool: UniRx.IScheduler
		static readonly IsCurrentThreadSchedulerScheduleRequired: boolean
		static readonly Now: any
		static readonly MainThread: UniRx.IScheduler
		static readonly MainThreadIgnoreTimeScale: UniRx.IScheduler
		static readonly MainThreadFixedUpdate: UniRx.IScheduler
		static readonly MainThreadEndOfFrame: UniRx.IScheduler
		static Normalize(timeSpan: any): any
		static Schedule(scheduler: UniRx.IScheduler, dueTime: any, action: (() => void)): System.IDisposable
		static Schedule(scheduler: UniRx.IScheduler, action: ((obj: (() => void)) => void)): System.IDisposable
		static Schedule(scheduler: UniRx.IScheduler, dueTime: any, action: ((obj: ((obj: any) => void)) => void)): System.IDisposable
		static Schedule(scheduler: UniRx.IScheduler, dueTime: any, action: ((obj: ((obj: any) => void)) => void)): System.IDisposable
		static SetDefaultForUnity(): void
	}
}
