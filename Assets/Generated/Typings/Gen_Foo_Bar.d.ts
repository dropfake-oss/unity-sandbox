// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Foo.Bar
declare namespace Foo {
	class Bar extends System.Object {
		constructor()
		GetData(): System.Object
	}
}
