// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.Exception
declare namespace System {
	class Exception extends System.Object {
		readonly Message: string
		readonly Data: any
		readonly InnerException: System.Exception
		readonly TargetSite: any
		readonly StackTrace: string
		HelpLink: string
		Source: string
		readonly HResult: number
		constructor()
		constructor(message: string)
		constructor(message: string, innerException: System.Exception)
		GetBaseException(): System.Exception
		ToString(): string
		GetObjectData(info: any, context: any): void
		GetType(): any
		Relevant(): System.Exception
		SelectedName(human: boolean): string
		DisplayName(): string
		HumanName(): string
		ToSummaryString(): string
		Throw(): void
	}
}
