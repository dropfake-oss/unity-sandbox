// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableEnableTrigger
declare namespace UniRx.Triggers {
	class ObservableEnableTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnEnableAsObservable(): System.IObservable<UniRx.Unit>
		OnDisableAsObservable(): System.IObservable<UniRx.Unit>
	}
}
