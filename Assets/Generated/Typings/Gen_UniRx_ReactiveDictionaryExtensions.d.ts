// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveDictionaryExtensions
declare namespace UniRx {
	abstract class ReactiveDictionaryExtensions extends System.Object {
		static ToReactiveDictionary<TKey, TValue>(dictionary: any): UniRx.ReactiveDictionary<TKey, TValue>
	}
}
