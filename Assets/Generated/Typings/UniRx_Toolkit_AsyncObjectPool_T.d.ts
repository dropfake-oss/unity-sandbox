// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Toolkit.AsyncObjectPool`1
declare namespace UniRx.Toolkit {
	abstract class AsyncObjectPool<T> extends System.Object implements System.IDisposable {
		readonly Count: number
		RentAsync(): System.IObservable<T>
		Return(instance: T): void
		Shrink(instanceCountRatio: number, minSize: number, callOnBeforeRent: boolean): void
		StartShrinkTimer(checkInterval: any, instanceCountRatio: number, minSize: number, callOnBeforeRent: boolean): System.IDisposable
		Clear(callOnBeforeRent: boolean): void
		PreloadAsync(preloadCount: number, threshold: number): System.IObservable<UniRx.Unit>
		Dispose(): void
	}
}
