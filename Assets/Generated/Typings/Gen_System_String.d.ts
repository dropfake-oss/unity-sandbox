// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.String
declare namespace System {
	class String extends System.Object {
		static readonly Empty: string
		readonly Length: number
		readonly Chars: string
		constructor(value: string[])
		constructor(value: string[], startIndex: number, length: number)
		constructor(value: any)
		constructor(value: any, startIndex: number, length: number)
		constructor(value: any)
		constructor(value: any, startIndex: number, length: number)
		constructor(value: any, startIndex: number, length: number, enc: any)
		constructor(c: string, count: number)
		constructor(value: any)
		static Compare(strA: string, strB: string): number
		static Compare(strA: string, strB: string, ignoreCase: boolean): number
		static Compare(strA: string, strB: string, comparisonType: any): number
		static Compare(strA: string, strB: string, culture: any, options: any): number
		static Compare(strA: string, strB: string, ignoreCase: boolean, culture: any): number
		static Compare(strA: string, indexA: number, strB: string, indexB: number, length: number): number
		static Compare(strA: string, indexA: number, strB: string, indexB: number, length: number, ignoreCase: boolean): number
		static Compare(strA: string, indexA: number, strB: string, indexB: number, length: number, ignoreCase: boolean, culture: any): number
		static Compare(strA: string, indexA: number, strB: string, indexB: number, length: number, culture: any, options: any): number
		static Compare(strA: string, indexA: number, strB: string, indexB: number, length: number, comparisonType: any): number
		static CompareOrdinal(strA: string, strB: string): number
		static CompareOrdinal(strA: string, indexA: number, strB: string, indexB: number, length: number): number
		CompareTo(value: System.Object): number
		CompareTo(strB: string): number
		EndsWith(value: string): boolean
		EndsWith(value: string, comparisonType: any): boolean
		EndsWith(value: string, ignoreCase: boolean, culture: any): boolean
		EndsWith(value: string): boolean
		Equals(obj: System.Object): boolean
		Equals(value: string): boolean
		Equals(value: string, comparisonType: any): boolean
		static Equals(a: string, b: string): boolean
		static Equals(a: string, b: string, comparisonType: any): boolean
		GetHashCode(): number
		GetHashCode(comparisonType: any): number
		StartsWith(value: string): boolean
		StartsWith(value: string, comparisonType: any): boolean
		StartsWith(value: string, ignoreCase: boolean, culture: any): boolean
		StartsWith(value: string): boolean
		static Concat(arg0: System.Object): string
		static Concat(arg0: System.Object, arg1: System.Object): string
		static Concat(arg0: System.Object, arg1: System.Object, arg2: System.Object): string
		static Concat(...args: System.Object[]): string
		static Concat<T>(values: any): string
		static Concat(values: any): string
		static Concat(str0: string, str1: string): string
		static Concat(str0: string, str1: string, str2: string): string
		static Concat(str0: string, str1: string, str2: string, str3: string): string
		static Concat(...values: string[]): string
		static Format(format: string, arg0: System.Object): string
		static Format(format: string, arg0: System.Object, arg1: System.Object): string
		static Format(format: string, arg0: System.Object, arg1: System.Object, arg2: System.Object): string
		static Format(format: string, ...args: System.Object[]): string
		static Format(provider: any, format: string, arg0: System.Object): string
		static Format(provider: any, format: string, arg0: System.Object, arg1: System.Object): string
		static Format(provider: any, format: string, arg0: System.Object, arg1: System.Object, arg2: System.Object): string
		static Format(provider: any, format: string, ...args: System.Object[]): string
		Insert(startIndex: number, value: string): string
		static Join(separator: string, ...value: string[]): string
		static Join(separator: string, ...values: System.Object[]): string
		static Join<T>(separator: string, values: any): string
		static Join(separator: string, value: string[], startIndex: number, count: number): string
		static Join(separator: string, ...value: string[]): string
		static Join(separator: string, ...values: System.Object[]): string
		static Join<T>(separator: string, values: any): string
		static Join(separator: string, values: any): string
		static Join(separator: string, value: string[], startIndex: number, count: number): string
		PadLeft(totalWidth: number): string
		PadLeft(totalWidth: number, paddingChar: string): string
		PadRight(totalWidth: number): string
		PadRight(totalWidth: number, paddingChar: string): string
		Remove(startIndex: number, count: number): string
		Remove(startIndex: number): string
		Replace(oldValue: string, newValue: string, ignoreCase: boolean, culture: any): string
		Replace(oldValue: string, newValue: string, comparisonType: any): string
		Replace(oldChar: string, newChar: string): string
		Replace(oldValue: string, newValue: string): string
		Split(separator: string, options: any): string[]
		Split(separator: string, count: number, options: any): string[]
		Split(...separator: string[]): string[]
		Split(separator: string[], count: number): string[]
		Split(separator: string[], options: any): string[]
		Split(separator: string[], count: number, options: any): string[]
		Split(separator: string, options: any): string[]
		Split(separator: string, count: number, options: any): string[]
		Split(separator: string[], options: any): string[]
		Split(separator: string[], count: number, options: any): string[]
		Substring(startIndex: number): string
		Substring(startIndex: number, length: number): string
		ToLower(): string
		ToLower(culture: any): string
		ToLowerInvariant(): string
		ToUpper(): string
		ToUpper(culture: any): string
		ToUpperInvariant(): string
		Trim(): string
		Trim(trimChar: string): string
		Trim(...trimChars: string[]): string
		TrimStart(): string
		TrimStart(trimChar: string): string
		TrimStart(...trimChars: string[]): string
		TrimEnd(): string
		TrimEnd(trimChar: string): string
		TrimEnd(...trimChars: string[]): string
		Contains(value: string): boolean
		Contains(value: string, comparisonType: any): boolean
		Contains(value: string): boolean
		Contains(value: string, comparisonType: any): boolean
		IndexOf(value: string): number
		IndexOf(value: string, startIndex: number): number
		IndexOf(value: string, comparisonType: any): number
		IndexOf(value: string, startIndex: number, count: number): number
		IndexOfAny(anyOf: string[]): number
		IndexOfAny(anyOf: string[], startIndex: number): number
		IndexOfAny(anyOf: string[], startIndex: number, count: number): number
		IndexOf(value: string): number
		IndexOf(value: string, startIndex: number): number
		IndexOf(value: string, startIndex: number, count: number): number
		IndexOf(value: string, comparisonType: any): number
		IndexOf(value: string, startIndex: number, comparisonType: any): number
		IndexOf(value: string, startIndex: number, count: number, comparisonType: any): number
		LastIndexOf(value: string): number
		LastIndexOf(value: string, startIndex: number): number
		LastIndexOf(value: string, startIndex: number, count: number): number
		LastIndexOfAny(anyOf: string[]): number
		LastIndexOfAny(anyOf: string[], startIndex: number): number
		LastIndexOfAny(anyOf: string[], startIndex: number, count: number): number
		LastIndexOf(value: string): number
		LastIndexOf(value: string, startIndex: number): number
		LastIndexOf(value: string, startIndex: number, count: number): number
		LastIndexOf(value: string, comparisonType: any): number
		LastIndexOf(value: string, startIndex: number, comparisonType: any): number
		LastIndexOf(value: string, startIndex: number, count: number, comparisonType: any): number
		static Create<TState>(length: number, state: TState, action: ((span: any, arg: TState) => void)): string
		Clone(): System.Object
		static Copy(str: string): string
		CopyTo(sourceIndex: number, destination: string[], destinationIndex: number, count: number): void
		ToCharArray(): string[]
		ToCharArray(startIndex: number, length: number): string[]
		static IsNullOrEmpty(value: string): boolean
		static IsNullOrWhiteSpace(value: string): boolean
		ToString(): string
		ToString(provider: any): string
		GetEnumerator(): any
		GetTypeCode(): any
		IsNormalized(): boolean
		IsNormalized(normalizationForm: any): boolean
		Normalize(): string
		Normalize(normalizationForm: any): string
		static Concat(arg0: System.Object, arg1: System.Object, arg2: System.Object, arg3: System.Object): string
		static Intern(str: string): string
		static IsInterned(str: string): string
		DecodeHexString(): Buffer
		AsSpan(): any
		AsSpan(start: number): any
		AsSpan(start: number, length: number): any
		AsMemory(): any
		AsMemory(start: number): any
		AsMemory(startIndex: any): any
		AsMemory(start: number, length: number): any
		AsMemory(range: any): any
		ToPascalCase(): string
		ToCamelCase(): string
		ToKebabCase(): string
		ToTrainCase(): string
		ToSnakeCase(): string
		EndsWithIgnoreCaseFast(b: string): boolean
		StartsWithIgnoreCaseFast(b: string): boolean
		ToNPath(): any
		Capitalize(): string
		Lowerize(): string
		EscapeBackslashes(): string
		DeleteRecursive(): void
		NormalizePath(): string
		ConvertSeparatorsToWindows(): string
		ConvertSeparatorsToUnity(): string
		TrimTrailingSlashes(): string
		Escape(value: string): string
		IsInPackage(): boolean
		IsInAssetsFolder(): boolean
		GetStringView(startIndex: number): any
		GetStringView(startIndex: number, endIndex: number): any
		GetStringView(): any
		GetWordView(startIndex: number): any
		GetHashCode64(): number
		IsNormalized(): boolean
		IsNormalized(normalizationForm: any): boolean
		Normalize(): string
		Normalize(normalizationForm: any): string
		ToNPath(): any
		FromJson(): T
		FromJson(type: any): System.Object
		InQuotes(): string
		ForwardSlashes(): string
		BackwardsSlashes(): string
		NormalizePathSeparators(): string
		NormalizeWindowsToUnix(): string
		MakeAbsolutePath(): string
		ContainsInsensitive(needle: string): boolean
		AllIndexesOf(needle: string): any
		Filter(letters: boolean, numbers: boolean, whitespace: boolean, symbols: boolean, punctuation: boolean): string
		FilterReplace(replacement: string, merge: boolean, letters: boolean, numbers: boolean, whitespace: boolean, symbols: boolean, punctuation: boolean): string
		Prettify(): string
		SplitWords(separator: string): string
		RemoveConsecutiveCharacters(c: string): string
		ReplaceMultiple(haystacks: any, replacement: string): string
		Truncate(maxLength: number, suffix: string): string
		TrimEnd(value: string): string
		TrimStart(value: string): string
		FirstCharacterToLower(): string
		FirstCharacterToUpper(): string
		PartBefore(c: string): string
		PartAfter(c: string): string
		PartsAround(c: string, before: any, after: any): void
		EndsWith(c: string): boolean
		StartsWith(c: string): boolean
		Contains(c: string): boolean
		NullIfEmpty(): string
		CountIndices(c: string): number
		NormalizePath(): string
		ToIntArray(): number[]
		NormalizePath(): string
		FirstCharToUpper(): string
		ToCamelCase(): string
		ToPascalCase(): string
		FromCamelCase(separator: string): string
		Slice(start: number, end: number): string
		CharCodeAt(index: number): string
		FormatWith(provider: any, arg0?: System.Object): string
		FormatWith(provider: any, arg0?: System.Object, arg1?: System.Object): string
		FormatWith(provider: any, arg0?: System.Object, arg1?: System.Object, arg2?: System.Object): string
		FormatWith(provider: any, arg0?: System.Object, arg1?: System.Object, arg2?: System.Object, arg3?: System.Object): string
		StartsWith(value: string): boolean
		EndsWith(value: string): boolean
		Trim(start: number, length: number): string
		ContainsInvariantCultureIgnoreCase(toCheck: string): boolean
		ToNPath(): any
		SplitOnCommas(): string[]
		Indent(friendlyForamt: boolean, indentation: number): string
		NewLineIndent(friendlyFormat: boolean, indentation: number): string
		TrimFirstLine(): string
		ParseRuntime(): any
	}
}
