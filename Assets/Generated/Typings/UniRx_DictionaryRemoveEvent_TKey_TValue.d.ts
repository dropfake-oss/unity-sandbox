// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.DictionaryRemoveEvent`2
declare namespace UniRx {
	class DictionaryRemoveEvent<TKey, TValue> extends System.Object {
		readonly Key: TKey
		readonly Value: TValue
		constructor(key: TKey, value: TValue)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.DictionaryRemoveEvent<TKey, TValue>): boolean
	}
}
