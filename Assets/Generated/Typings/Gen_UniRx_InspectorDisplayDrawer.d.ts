// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InspectorDisplayDrawer
declare namespace UniRx {
	class InspectorDisplayDrawer extends System.Object {
		constructor()
		OnGUI(position: any, property: any, label: any): void
		GetPropertyHeight(property: any, label: any): number
	}
}
