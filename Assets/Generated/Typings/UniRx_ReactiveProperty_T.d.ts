// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveProperty`1
declare namespace UniRx {
	class ReactiveProperty<T> extends System.Object implements System.IObservable<T>, UniRx.IReadOnlyReactiveProperty<T>, UniRx.IReactiveProperty<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		Value: T
		readonly HasValue: boolean
		constructor()
		constructor(initialValue: T)
		SetValueAndForceNotify(value: T): void
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Dispose(): void
		ToString(): string
		IsRequiredSubscribeOnCurrentThread(): boolean
	}
}
