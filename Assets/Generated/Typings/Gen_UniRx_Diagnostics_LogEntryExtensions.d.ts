// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.LogEntryExtensions
declare namespace UniRx.Diagnostics {
	abstract class LogEntryExtensions extends System.Object {
		static LogToUnityDebug(source: System.IObservable<UniRx.Diagnostics.LogEntry>): System.IDisposable
	}
}
