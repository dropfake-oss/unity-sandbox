// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.MicroCoroutine
declare namespace UniRx.InternalUtil {
	class MicroCoroutine extends System.Object {
		constructor(unhandledExceptionCallback: ((obj: System.Exception) => void))
		AddCoroutine(enumerator: any): void
		Run(): void
	}
}
