// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Subject`1
declare namespace UniRx {
	class Subject<T> extends System.Object implements UniRx.ISubject<T, T>, UniRx.ISubject<T>, System.IObservable<T>, System.IObserver<T>, UniRx.IOptimizedObservable<T>, System.IDisposable {
		readonly HasObservers: boolean
		constructor()
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Subscribe(observer: System.IObserver<T>): System.IDisposable
		Dispose(): void
		IsRequiredSubscribeOnCurrentThread(): boolean
	}
}
