// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableScrollTrigger
declare namespace UniRx.Triggers {
	class ObservableScrollTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnScrollAsObservable(): System.IObservable<any>
	}
}
