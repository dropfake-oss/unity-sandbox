// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ReactiveDictionary`2
declare namespace UniRx {
	class ReactiveDictionary<TKey, TValue> extends Array implements UniRx.IReadOnlyReactiveDictionary<TKey, TValue>, UniRx.IReactiveDictionary<TKey, TValue>, System.IDisposable {
		readonly Count: number
		readonly Keys: any
		readonly Values: any
		constructor()
		constructor(comparer: any)
		constructor(innerDictionary: any)
		Add(key: TKey, value: TValue): void
		Clear(): void
		Remove(key: TKey): boolean
		ContainsKey(key: TKey): boolean
		TryGetValue(key: TKey, value: any): boolean
		GetEnumerator(): any
		Dispose(): void
		ObserveCountChanged(notifyCurrentCount: boolean): System.IObservable<number>
		ObserveReset(): System.IObservable<UniRx.Unit>
		ObserveAdd(): System.IObservable<UniRx.DictionaryAddEvent<TKey, TValue>>
		ObserveRemove(): System.IObservable<UniRx.DictionaryRemoveEvent<TKey, TValue>>
		ObserveReplace(): System.IObservable<UniRx.DictionaryReplaceEvent<TKey, TValue>>
		GetObjectData(info: any, context: any): void
		OnDeserialization(sender: System.Object): void
	}
}
