// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Disposable
declare namespace UniRx {
	abstract class Disposable extends System.Object {
		static readonly Empty: System.IDisposable
		static Create(disposeAction: (() => void)): System.IDisposable
		static CreateWithState<TState>(state: TState, disposeAction: ((obj: TState) => void)): System.IDisposable
	}
}
