// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UnityEngine.CoreModule
// Type: UnityEngine.Object
declare namespace UnityEngine {
	class Object extends System.Object {
		/** The name of the object.
		 */
		name: string
		/** Should the object be hidden, saved with the Scene or modifiable by the user?
		 */
		hideFlags: any
		constructor()
		GetInstanceID(): number
		GetHashCode(): number
		Equals(other: System.Object): boolean
		/** Clones the object original and returns the clone.
		 * @param original An existing object that you want to make a copy of.
		 * @param position Position for the new object.
		 * @param rotation Orientation of the new object.
		 * @param parent Parent that will be assigned to the new object.
		 * @param instantiateInWorldSpace When you assign a parent Object, pass true to position the new object directly in world space. Pass false to set the Object’s position relative to its new parent.
		 * @returns The instantiated clone. 
		 */
		static Instantiate(original: UnityEngine.Object, position: UnityEngine.Vector3, rotation: UnityEngine.Quaternion): UnityEngine.Object
		/** Clones the object original and returns the clone.
		 * @param original An existing object that you want to make a copy of.
		 * @param position Position for the new object.
		 * @param rotation Orientation of the new object.
		 * @param parent Parent that will be assigned to the new object.
		 * @param instantiateInWorldSpace When you assign a parent Object, pass true to position the new object directly in world space. Pass false to set the Object’s position relative to its new parent.
		 * @returns The instantiated clone. 
		 */
		static Instantiate(original: UnityEngine.Object, position: UnityEngine.Vector3, rotation: UnityEngine.Quaternion, parent: any): UnityEngine.Object
		/** Clones the object original and returns the clone.
		 * @param original An existing object that you want to make a copy of.
		 * @param position Position for the new object.
		 * @param rotation Orientation of the new object.
		 * @param parent Parent that will be assigned to the new object.
		 * @param instantiateInWorldSpace When you assign a parent Object, pass true to position the new object directly in world space. Pass false to set the Object’s position relative to its new parent.
		 * @returns The instantiated clone. 
		 */
		static Instantiate(original: UnityEngine.Object): UnityEngine.Object
		/** Clones the object original and returns the clone.
		 * @param original An existing object that you want to make a copy of.
		 * @param position Position for the new object.
		 * @param rotation Orientation of the new object.
		 * @param parent Parent that will be assigned to the new object.
		 * @param instantiateInWorldSpace When you assign a parent Object, pass true to position the new object directly in world space. Pass false to set the Object’s position relative to its new parent.
		 * @returns The instantiated clone. 
		 */
		static Instantiate(original: UnityEngine.Object, parent: any): UnityEngine.Object
		/** Clones the object original and returns the clone.
		 * @param original An existing object that you want to make a copy of.
		 * @param position Position for the new object.
		 * @param rotation Orientation of the new object.
		 * @param parent Parent that will be assigned to the new object.
		 * @param instantiateInWorldSpace When you assign a parent Object, pass true to position the new object directly in world space. Pass false to set the Object’s position relative to its new parent.
		 * @returns The instantiated clone. 
		 */
		static Instantiate(original: UnityEngine.Object, parent: any, instantiateInWorldSpace: boolean): UnityEngine.Object
		static Instantiate<T>(original: T): T
		static Instantiate<T>(original: T, position: UnityEngine.Vector3, rotation: UnityEngine.Quaternion): T
		static Instantiate<T>(original: T, position: UnityEngine.Vector3, rotation: UnityEngine.Quaternion, parent: any): T
		static Instantiate<T>(original: T, parent: any): T
		static Instantiate<T>(original: T, parent: any, worldPositionStays: boolean): T
		/** Removes a GameObject, component or asset.
		 * @param obj The object to destroy.
		 * @param t The optional amount of time to delay before destroying the object.
		 */
		static Destroy(obj: UnityEngine.Object, t: number): void
		/** Removes a GameObject, component or asset.
		 * @param obj The object to destroy.
		 * @param t The optional amount of time to delay before destroying the object.
		 */
		static Destroy(obj: UnityEngine.Object): void
		/** Destroys the object obj immediately. You are strongly recommended to use Destroy instead.
		 * @param obj Object to be destroyed.
		 * @param allowDestroyingAssets Set to true to allow assets to be destroyed.
		 */
		static DestroyImmediate(obj: UnityEngine.Object, allowDestroyingAssets: boolean): void
		/** Destroys the object obj immediately. You are strongly recommended to use Destroy instead.
		 * @param obj Object to be destroyed.
		 * @param allowDestroyingAssets Set to true to allow assets to be destroyed.
		 */
		static DestroyImmediate(obj: UnityEngine.Object): void
		/** Gets a list of all loaded objects of Type type.
		 * @param type The type of object to find.
		 * @param includeInactive If true, components attached to inactive GameObjects are also included.
		 * @returns The array of objects found matching the type specified. 
		 */
		static FindObjectsOfType(type: any): UnityEngine.Object[]
		/** Gets a list of all loaded objects of Type type.
		 * @param type The type of object to find.
		 * @param includeInactive If true, components attached to inactive GameObjects are also included.
		 * @returns The array of objects found matching the type specified. 
		 */
		static FindObjectsOfType(type: any, includeInactive: boolean): UnityEngine.Object[]
		/** Do not destroy the target Object when loading a new Scene.
		 * @param target An Object not destroyed on Scene change.
		 */
		static DontDestroyOnLoad(target: UnityEngine.Object): void
		static FindObjectsOfType<T>(): T[]
		static FindObjectsOfType<T>(includeInactive: boolean): T[]
		static FindObjectOfType<T>(): T
		static FindObjectOfType<T>(includeInactive: boolean): T
		/** Returns the first active loaded object of Type type.
		 * @param type The type of object to find.
		 * @returns Object The first active loaded object that matches the specified type. It returns null if no Object matches the type. 
		 */
		static FindObjectOfType(type: any): UnityEngine.Object
		/** Returns the first active loaded object of Type type.
		 * @param type The type of object to find.
		 * @returns Object The first active loaded object that matches the specified type. It returns null if no Object matches the type. 
		 */
		static FindObjectOfType(type: any, includeInactive: boolean): UnityEngine.Object
		ToString(): string
		IsSceneObject(): boolean
		IsPrefab(): boolean
		IsComponentHolder(): boolean
		GameObject(): any
		AddComponent(): T
		GetOrAddComponent(): T
		GetComponent(): T
		GetComponentInChildren(): T
		GetComponentInParent(): T
		GetComponents(): T[]
		GetComponentsInChildren(): T[]
		GetComponentsInParent(): T[]
		GetComponent(type: any): any
		GetComponentInChildren(type: any): any
		GetComponentInParent(type: any): any
		GetComponents(type: any): any[]
		GetComponentsInChildren(type: any): any[]
		GetComponentsInParent(type: any): any[]
		IsDestroyed(): boolean
		ToSafeString(): string
		Icon(): any
		GetPrefabDefinition(): UnityEngine.Object
		IsPrefabInstance(): boolean
		IsPrefabDefinition(): boolean
		IsConnectedPrefabInstance(): boolean
		IsDisconnectedPrefabInstance(): boolean
		IsSceneBound(): boolean
	}
}
