// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ObservableYieldInstruction`1
declare namespace UniRx {
	class ObservableYieldInstruction<T> extends System.Object implements System.IDisposable {
		readonly HasError: boolean
		readonly HasResult: boolean
		readonly IsCanceled: boolean
		readonly IsDone: boolean
		readonly Result: T
		readonly Error: System.Exception
		constructor(source: System.IObservable<T>, reThrowOnError: boolean, cancel: any)
		Dispose(): void
	}
}
