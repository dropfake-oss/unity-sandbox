// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IReactiveProperty`1
declare namespace UniRx {
	interface IReactiveProperty<T> extends System.IObservable<T>, UniRx.IReadOnlyReactiveProperty<T> {
		Value: T
		ToAsyncReactiveCommand(): UniRx.AsyncReactiveCommand
		ToAsyncReactiveCommand(): UniRx.AsyncReactiveCommand<T>
		WaitUntilValueChangedAsync(cancellationToken: any): any
		GetAwaiter(): any
		SkipLatestValueOnSubscribe(): System.IObservable<T>
	}
}
