// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Timestamped
declare namespace UniRx {
	abstract class Timestamped extends System.Object {
		static Create<T>(value: T, timestamp: any): UniRx.Timestamped<T>
	}
}
