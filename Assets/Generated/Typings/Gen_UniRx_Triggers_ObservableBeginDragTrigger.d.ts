// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableBeginDragTrigger
declare namespace UniRx.Triggers {
	class ObservableBeginDragTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnBeginDragAsObservable(): System.IObservable<any>
	}
}
