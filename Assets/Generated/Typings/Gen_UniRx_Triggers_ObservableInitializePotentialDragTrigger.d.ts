// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableInitializePotentialDragTrigger
declare namespace UniRx.Triggers {
	class ObservableInitializePotentialDragTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnInitializePotentialDragAsObservable(): System.IObservable<any>
	}
}
