// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IMessageBroker
declare namespace UniRx {
	interface IMessageBroker extends UniRx.IMessagePublisher, UniRx.IMessageReceiver {
	}
}
