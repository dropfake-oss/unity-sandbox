// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.WebRequestExtensions
declare namespace UniRx {
	abstract class WebRequestExtensions extends System.Object {
		static GetResponseAsObservable(request: any): System.IObservable<any>
		static GetResponseAsObservable(request: any): System.IObservable<any>
		static GetRequestStreamAsObservable(request: any): System.IObservable<any>
	}
}
