// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableJointTrigger
declare namespace UniRx.Triggers {
	class ObservableJointTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnJointBreakAsObservable(): System.IObservable<number>
		OnJointBreak2DAsObservable(): System.IObservable<any>
	}
}
