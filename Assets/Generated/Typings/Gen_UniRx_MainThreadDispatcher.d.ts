// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.MainThreadDispatcher
declare namespace UniRx {
	class MainThreadDispatcher extends UnityEngine.Object {
		static cullingMode: UniRx.MainThreadDispatcher.CullingMode
		static readonly InstanceName: string
		static readonly IsInitialized: boolean
		static readonly IsInMainThread: boolean
		constructor()
		static Post(action: ((obj: System.Object) => void), state: System.Object): void
		static Send(action: ((obj: System.Object) => void), state: System.Object): void
		static UnsafeSend(action: (() => void)): void
		static UnsafeSend<T>(action: ((obj: T) => void), state: T): void
		static SendStartCoroutine(routine: any): void
		static StartUpdateMicroCoroutine(routine: any): void
		static StartFixedUpdateMicroCoroutine(routine: any): void
		static StartEndOfFrameMicroCoroutine(routine: any): void
		static StartCoroutine(routine: any): any
		static RegisterUnhandledExceptionCallback(exceptionCallback: ((obj: System.Exception) => void)): void
		static Initialize(): void
		static CullAllExcessDispatchers(): void
		static UpdateAsObservable(): System.IObservable<UniRx.Unit>
		static LateUpdateAsObservable(): System.IObservable<UniRx.Unit>
		static OnApplicationFocusAsObservable(): System.IObservable<boolean>
		static OnApplicationPauseAsObservable(): System.IObservable<boolean>
		static OnApplicationQuitAsObservable(): System.IObservable<UniRx.Unit>
	}
}
