// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableRectTransformTrigger
declare namespace UniRx.Triggers {
	class ObservableRectTransformTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnRectTransformDimensionsChangeAsObservable(): System.IObservable<UniRx.Unit>
		OnRectTransformRemovedAsObservable(): System.IObservable<UniRx.Unit>
	}
}
