// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Operators.CombineLatestFunc`4
declare namespace UniRx.Operators {
	class CombineLatestFunc<T1, T2, T3, TR> extends System.Object {
		constructor(object: System.Object, method: any)
		Invoke(arg1: T1, arg2: T2, arg3: T3): TR
		BeginInvoke(arg1: T1, arg2: T2, arg3: T3, callback: ((ar: any) => void), object: System.Object): any
		EndInvoke(result: any): TR
	}
}
