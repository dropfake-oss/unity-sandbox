// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.QuaternionReactiveProperty
declare namespace UniRx {
	class QuaternionReactiveProperty extends UniRx.ReactiveProperty<UnityEngine.Quaternion> implements System.IObservable<UnityEngine.Quaternion>, UniRx.IReadOnlyReactiveProperty<UnityEngine.Quaternion>, UniRx.IReactiveProperty<UnityEngine.Quaternion>, UniRx.IOptimizedObservable<UnityEngine.Quaternion>, System.IDisposable {
		constructor()
		constructor(initialValue: UnityEngine.Quaternion)
	}
}
