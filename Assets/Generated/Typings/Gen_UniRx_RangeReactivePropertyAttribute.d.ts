// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.RangeReactivePropertyAttribute
declare namespace UniRx {
	class RangeReactivePropertyAttribute extends System.Object {
		readonly Min: number
		readonly Max: number
		constructor(min: number, max: number)
	}
}
