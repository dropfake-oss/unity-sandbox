// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Concrete.Stuff.Boom
declare namespace Concrete.Stuff {
	class Boom extends System.Object implements Lord.Dance.IAction, Rabbit.Fudge.IEffect, Hooligan.Foo.IBehaviour {
		readonly Name: string
		constructor()
	}
}
