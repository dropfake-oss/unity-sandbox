// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AnimationCurveReactiveProperty
declare namespace UniRx {
	class AnimationCurveReactiveProperty extends UniRx.ReactiveProperty<any> implements System.IObservable<any>, UniRx.IReadOnlyReactiveProperty<any>, UniRx.IReactiveProperty<any>, UniRx.IOptimizedObservable<any>, System.IDisposable {
		constructor()
		constructor(initialValue: any)
	}
}
