// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.UnityEventExtensions
declare namespace UniRx {
	abstract class UnityEventExtensions extends System.Object {
		static AsObservable(unityEvent: any): System.IObservable<UniRx.Unit>
		static AsObservable<T>(unityEvent: any): System.IObservable<T>
		static AsObservable<T0, T1>(unityEvent: any): System.IObservable<any>
		static AsObservable<T0, T1, T2>(unityEvent: any): System.IObservable<any>
		static AsObservable<T0, T1, T2, T3>(unityEvent: any): System.IObservable<any>
	}
}
