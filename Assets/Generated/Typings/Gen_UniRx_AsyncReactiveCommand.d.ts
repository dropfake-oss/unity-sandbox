// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncReactiveCommand
declare namespace UniRx {
	class AsyncReactiveCommand extends UniRx.AsyncReactiveCommand<UniRx.Unit> implements UniRx.IAsyncReactiveCommand<UniRx.Unit> {
		constructor()
		constructor(canExecuteSource: System.IObservable<boolean>)
		constructor(sharedCanExecute: UniRx.IReactiveProperty<boolean>)
		Execute(): System.IDisposable
		WaitUntilExecuteAsync(cancellationToken: any): any
		GetAwaiter(): any
		BindTo(button: any): System.IDisposable
		BindToOnClick(button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
	}
}
