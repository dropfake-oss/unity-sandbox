// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ISubject`1
declare namespace UniRx {
	interface ISubject<T> extends UniRx.ISubject<T, T>, System.IObservable<T>, System.IObserver<T> {
		Synchronize(): UniRx.ISubject<T>
		Synchronize(gate: System.Object): UniRx.ISubject<T>
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
	}
}
