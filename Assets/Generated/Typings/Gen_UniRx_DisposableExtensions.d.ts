// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.DisposableExtensions
declare namespace UniRx {
	abstract class DisposableExtensions extends System.Object {
		static AddTo<T>(disposable: T, container: any): T
		static AddTo<T>(disposable: T, gameObject: any): T
		static AddTo<T>(disposable: T, gameObjectComponent: any): T
		static AddTo<T>(disposable: T, container: any, gameObject: any): T
		static AddTo<T>(disposable: T, container: any, gameObjectComponent: any): T
	}
}
