// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservableTrigger2DTrigger
declare namespace UniRx.Triggers {
	class ObservableTrigger2DTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnTriggerEnter2DAsObservable(): System.IObservable<any>
		OnTriggerExit2DAsObservable(): System.IObservable<any>
		OnTriggerStay2DAsObservable(): System.IObservable<any>
	}
}
