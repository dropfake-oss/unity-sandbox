// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.SingleAssignmentDisposable
declare namespace UniRx {
	class SingleAssignmentDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		Disposable: System.IDisposable
		constructor()
		Dispose(): void
	}
}
