// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FMatrix
declare namespace FixedPointMath {
	class FMatrix extends System.Object {
		M11: FixedPointMath.Fixed
		M12: FixedPointMath.Fixed
		M13: FixedPointMath.Fixed
		M21: FixedPointMath.Fixed
		M22: FixedPointMath.Fixed
		M23: FixedPointMath.Fixed
		M31: FixedPointMath.Fixed
		M32: FixedPointMath.Fixed
		M33: FixedPointMath.Fixed
		static readonly Identity: FixedPointMath.FMatrix
		static readonly Zero: FixedPointMath.FMatrix
		readonly eulerAngles: FixedPointMath.FVector3
		constructor(m11: FixedPointMath.Fixed, m12: FixedPointMath.Fixed, m13: FixedPointMath.Fixed, m21: FixedPointMath.Fixed, m22: FixedPointMath.Fixed, m23: FixedPointMath.Fixed, m31: FixedPointMath.Fixed, m32: FixedPointMath.Fixed, m33: FixedPointMath.Fixed)
		static CreateFromYawPitchRoll(yaw: FixedPointMath.Fixed, pitch: FixedPointMath.Fixed, roll: FixedPointMath.Fixed): FixedPointMath.FMatrix
		static CreateRotationX(radians: FixedPointMath.Fixed): FixedPointMath.FMatrix
		static CreateRotationX(radians: FixedPointMath.Fixed, result: any): void
		static CreateRotationY(radians: FixedPointMath.Fixed): FixedPointMath.FMatrix
		static CreateRotationY(radians: FixedPointMath.Fixed, result: any): void
		static CreateRotationZ(radians: FixedPointMath.Fixed): FixedPointMath.FMatrix
		static CreateRotationZ(radians: FixedPointMath.Fixed, result: any): void
		static Multiply(matrix1: FixedPointMath.FMatrix, matrix2: FixedPointMath.FMatrix): FixedPointMath.FMatrix
		static Multiply(matrix1: any, matrix2: any, result: any): void
		static Add(matrix1: FixedPointMath.FMatrix, matrix2: FixedPointMath.FMatrix): FixedPointMath.FMatrix
		static Add(matrix1: any, matrix2: any, result: any): void
		static Inverse(matrix: FixedPointMath.FMatrix): FixedPointMath.FMatrix
		Determinant(): FixedPointMath.Fixed
		static Invert(matrix: any, result: any): void
		static Inverse(matrix: any, result: any): void
		static Multiply(matrix1: FixedPointMath.FMatrix, scaleFactor: FixedPointMath.Fixed): FixedPointMath.FMatrix
		static Multiply(matrix1: any, scaleFactor: FixedPointMath.Fixed, result: any): void
		static CreateFromLookAt(position: FixedPointMath.FVector3, target: FixedPointMath.FVector3): FixedPointMath.FMatrix
		static LookAt(forward: FixedPointMath.FVector3, upwards: FixedPointMath.FVector3): FixedPointMath.FMatrix
		static LookAt(forward: FixedPointMath.FVector3, upwards: FixedPointMath.FVector3, result: any): void
		static CreateFromQuaternion(quaternion: FixedPointMath.FQuaternion): FixedPointMath.FMatrix
		static CreateFromQuaternion(quaternion: any, result: any): void
		static Transpose(matrix: FixedPointMath.FMatrix): FixedPointMath.FMatrix
		static Transpose(matrix: any, result: any): void
		Trace(): FixedPointMath.Fixed
		Equals(obj: System.Object): boolean
		GetHashCode(): number
		static CreateFromAxisAngle(axis: any, angle: FixedPointMath.Fixed, result: any): void
		static AngleAxis(angle: FixedPointMath.Fixed, axis: FixedPointMath.FVector3): FixedPointMath.FMatrix
		ToString(): string
	}
}
