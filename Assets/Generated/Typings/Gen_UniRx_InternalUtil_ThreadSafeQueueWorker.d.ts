// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.ThreadSafeQueueWorker
declare namespace UniRx.InternalUtil {
	class ThreadSafeQueueWorker extends System.Object {
		constructor()
		Enqueue(action: ((obj: System.Object) => void), state: System.Object): void
		ExecuteAll(unhandledExceptionCallback: ((obj: System.Exception) => void)): void
	}
}
