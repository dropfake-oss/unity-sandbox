// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.InternalUtil.ListObserver`1
declare namespace UniRx.InternalUtil {
	class ListObserver<T> extends System.Object implements System.IObserver<T> {
		constructor(observers: UniRx.InternalUtil.ImmutableList<System.IObserver<T>>)
		OnCompleted(): void
		OnError(error: System.Exception): void
		OnNext(value: T): void
		Synchronize(): System.IObserver<T>
		Synchronize(gate: System.Object): System.IObserver<T>
	}
}
