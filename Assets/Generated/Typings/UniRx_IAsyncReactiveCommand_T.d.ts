// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IAsyncReactiveCommand`1
declare namespace UniRx {
	interface IAsyncReactiveCommand<T> {
		readonly CanExecute: UniRx.IReadOnlyReactiveProperty<boolean>
		Execute(parameter: T): System.IDisposable
		Subscribe(asyncAction: ((arg: T) => System.IObservable<UniRx.Unit>)): System.IDisposable
		WaitUntilExecuteAsync(cancellationToken: any): any
		GetAwaiter(): any
		BindTo(button: any): System.IDisposable
		BindToOnClick(button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
		WaitUntilExecuteAsync(cancellationToken: any): any
		GetAwaiter(): any
		BindTo(button: any): System.IDisposable
		BindToOnClick(button: any, asyncOnClick: ((arg: UniRx.Unit) => System.IObservable<UniRx.Unit>)): System.IDisposable
	}
}
