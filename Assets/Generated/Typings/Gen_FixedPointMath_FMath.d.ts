// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: FixedPointMath
// Type: FixedPointMath.FMath
declare namespace FixedPointMath {
	class FMath extends System.Object {
		static Pi: FixedPointMath.Fixed
		static PiOver2: FixedPointMath.Fixed
		static Epsilon: FixedPointMath.Fixed
		static Deg2Rad: FixedPointMath.Fixed
		static Rad2Deg: FixedPointMath.Fixed
		constructor()
		static Sqrt(number: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Max(val1: FixedPointMath.Fixed, val2: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Min(val1: FixedPointMath.Fixed, val2: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Max(val1: FixedPointMath.Fixed, val2: FixedPointMath.Fixed, val3: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Clamp(value: FixedPointMath.Fixed, min: FixedPointMath.Fixed, max: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Absolute(matrix: any, result: any): void
		static Sin(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Cos(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Tan(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Asin(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Acos(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Atan(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Atan2(y: FixedPointMath.Fixed, x: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Floor(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Ceiling(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Round(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Sign(value: FixedPointMath.Fixed): number
		static Abs(value: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Barycentric(value1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed, value3: FixedPointMath.Fixed, amount1: FixedPointMath.Fixed, amount2: FixedPointMath.Fixed): FixedPointMath.Fixed
		static CatmullRom(value1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed, value3: FixedPointMath.Fixed, value4: FixedPointMath.Fixed, amount: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Distance(value1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Hermite(value1: FixedPointMath.Fixed, tangent1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed, tangent2: FixedPointMath.Fixed, amount: FixedPointMath.Fixed): FixedPointMath.Fixed
		static Lerp(value1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed, amount: FixedPointMath.Fixed): FixedPointMath.Fixed
		static SmoothStep(value1: FixedPointMath.Fixed, value2: FixedPointMath.Fixed, amount: FixedPointMath.Fixed): FixedPointMath.Fixed
		static InverseLerp(a: FixedPointMath.Fixed, b: FixedPointMath.Fixed, value: FixedPointMath.Fixed): FixedPointMath.Fixed
	}
}
