// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IAsyncMessageBroker
declare namespace UniRx {
	interface IAsyncMessageBroker extends UniRx.IAsyncMessagePublisher, UniRx.IAsyncMessageReceiver {
	}
}
