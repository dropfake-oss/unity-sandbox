// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: mscorlib
// Type: System.Single
declare namespace System {
	class Single extends System.Object {
		static readonly MinValue: number
		static readonly Epsilon: number
		static readonly MaxValue: number
		static readonly PositiveInfinity: number
		static readonly NegativeInfinity: number
		static readonly NaN: number
		static IsFinite(f: number): boolean
		static IsInfinity(f: number): boolean
		static IsNaN(f: number): boolean
		static IsNegative(f: number): boolean
		static IsNegativeInfinity(f: number): boolean
		static IsNormal(f: number): boolean
		static IsPositiveInfinity(f: number): boolean
		static IsSubnormal(f: number): boolean
		CompareTo(value: System.Object): number
		CompareTo(value: number): number
		Equals(obj: System.Object): boolean
		Equals(obj: number): boolean
		GetHashCode(): number
		ToString(): string
		ToString(provider: any): string
		ToString(format: string): string
		ToString(format: string, provider: any): string
		TryFormat(destination: any, charsWritten: any, format: any, provider: any): boolean
		static Parse(s: string): number
		static Parse(s: string, style: any): number
		static Parse(s: string, provider: any): number
		static Parse(s: string, style: any, provider: any): number
		static Parse(s: any, style: any, provider: any): number
		static TryParse(s: string, result: any): boolean
		static TryParse(s: any, result: any): boolean
		static TryParse(s: string, style: any, provider: any, result: any): boolean
		static TryParse(s: any, style: any, provider: any, result: any): boolean
		GetTypeCode(): any
		/** An extension method for Assertions.Assert.AreApproximatelyEqual.
		 */
		MustBeApproximatelyEqual(expected: number): void
		/** An extension method for Assertions.Assert.AreApproximatelyEqual.
		 */
		MustBeApproximatelyEqual(expected: number, message: string): void
		/** An extension method for Assertions.Assert.AreApproximatelyEqual.
		 */
		MustBeApproximatelyEqual(expected: number, tolerance: number): void
		/** An extension method for Assertions.Assert.AreApproximatelyEqual.
		 */
		MustBeApproximatelyEqual(expected: number, tolerance: number, message: string): void
		/** An extension method for Assertions.Assert.AreNotApproximatelyEqual.
		 */
		MustNotBeApproximatelyEqual(expected: number): void
		/** An extension method for Assertions.Assert.AreNotApproximatelyEqual.
		 */
		MustNotBeApproximatelyEqual(expected: number, message: string): void
		/** An extension method for Assertions.Assert.AreNotApproximatelyEqual.
		 */
		MustNotBeApproximatelyEqual(expected: number, tolerance: number): void
		/** An extension method for Assertions.Assert.AreNotApproximatelyEqual.
		 */
		MustNotBeApproximatelyEqual(expected: number, tolerance: number, message: string): void
		Percent(): any
		Pt(): any
	}
}
