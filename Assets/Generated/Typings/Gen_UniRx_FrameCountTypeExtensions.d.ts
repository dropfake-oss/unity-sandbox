// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.FrameCountTypeExtensions
declare namespace UniRx {
	abstract class FrameCountTypeExtensions extends System.Object {
		static GetYieldInstruction(frameCountType: UniRx.FrameCountType): any
	}
}
