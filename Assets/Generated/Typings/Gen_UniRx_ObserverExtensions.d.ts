// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ObserverExtensions
declare namespace UniRx {
	abstract class ObserverExtensions extends System.Object {
		static Synchronize<T>(observer: System.IObserver<T>): System.IObserver<T>
		static Synchronize<T>(observer: System.IObserver<T>, gate: System.Object): System.IObserver<T>
	}
}
