// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.AsyncMessageBroker
declare namespace UniRx {
	class AsyncMessageBroker extends System.Object implements UniRx.IAsyncMessagePublisher, UniRx.IAsyncMessageReceiver, UniRx.IAsyncMessageBroker, System.IDisposable {
		static readonly Default: UniRx.IAsyncMessageBroker
		constructor()
		PublishAsync<T>(message: T): System.IObservable<UniRx.Unit>
		Subscribe<T>(asyncMessageReceiver: ((arg: T) => System.IObservable<UniRx.Unit>)): System.IDisposable
		Dispose(): void
	}
}
