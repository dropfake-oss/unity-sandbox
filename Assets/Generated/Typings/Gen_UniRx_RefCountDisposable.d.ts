// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.RefCountDisposable
declare namespace UniRx {
	class RefCountDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		constructor(disposable: System.IDisposable)
		GetDisposable(): System.IDisposable
		Dispose(): void
	}
}
