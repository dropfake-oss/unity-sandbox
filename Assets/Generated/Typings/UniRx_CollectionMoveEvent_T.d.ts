// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CollectionMoveEvent`1
declare namespace UniRx {
	class CollectionMoveEvent<T> extends System.Object {
		readonly OldIndex: number
		readonly NewIndex: number
		readonly Value: T
		constructor(oldIndex: number, newIndex: number, value: T)
		ToString(): string
		GetHashCode(): number
		Equals(other: UniRx.CollectionMoveEvent<T>): boolean
	}
}
