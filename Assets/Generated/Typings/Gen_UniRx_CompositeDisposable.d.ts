// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.CompositeDisposable
declare namespace UniRx {
	class CompositeDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly Count: number
		readonly IsReadOnly: boolean
		readonly IsDisposed: boolean
		constructor()
		constructor(capacity: number)
		constructor(...disposables: System.IDisposable[])
		constructor(disposables: any)
		Add(item: System.IDisposable): void
		Remove(item: System.IDisposable): boolean
		Dispose(): void
		Clear(): void
		Contains(item: System.IDisposable): boolean
		CopyTo(array: System.IDisposable[], arrayIndex: number): void
		GetEnumerator(): any
	}
}
