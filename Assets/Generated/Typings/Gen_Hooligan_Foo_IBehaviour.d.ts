// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: Assembly-CSharp
// Type: Hooligan.Foo.IBehaviour
declare namespace Hooligan.Foo {
	interface IBehaviour {
		readonly Name: string
	}
}
