// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Observable
declare namespace UniRx {
	abstract class Observable extends System.Object {
		static Scan<TSource>(source: System.IObservable<TSource>, accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		static Scan<TSource, TAccumulate>(source: System.IObservable<TSource>, seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		static Aggregate<TSource>(source: System.IObservable<TSource>, accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		static Aggregate<TSource, TAccumulate>(source: System.IObservable<TSource>, seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		static Aggregate<TSource, TAccumulate, TResult>(source: System.IObservable<TSource>, seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		static GetAwaiter<TSource>(source: System.IObservable<TSource>): UniRx.AsyncSubject<TSource>
		static GetAwaiter<TSource>(source: UniRx.IConnectableObservable<TSource>): UniRx.AsyncSubject<TSource>
		static GetAwaiter<TSource>(source: System.IObservable<TSource>, cancellationToken: any): UniRx.AsyncSubject<TSource>
		static GetAwaiter<TSource>(source: UniRx.IConnectableObservable<TSource>, cancellationToken: any): UniRx.AsyncSubject<TSource>
		static Multicast<T>(source: System.IObservable<T>, subject: UniRx.ISubject<T>): UniRx.IConnectableObservable<T>
		static Publish<T>(source: System.IObservable<T>): UniRx.IConnectableObservable<T>
		static Publish<T>(source: System.IObservable<T>, initialValue: T): UniRx.IConnectableObservable<T>
		static PublishLast<T>(source: System.IObservable<T>): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, bufferSize: number): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, window: any): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		static Replay<T>(source: System.IObservable<T>, bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<T>
		static RefCount<T>(source: UniRx.IConnectableObservable<T>): System.IObservable<T>
		static Share<T>(source: System.IObservable<T>): System.IObservable<T>
		static Wait<T>(source: System.IObservable<T>): T
		static Wait<T>(source: System.IObservable<T>, timeout: any): T
		static Concat<TSource>(...sources: System.IObservable<TSource>[]): System.IObservable<TSource>
		static Concat<TSource>(sources: any): System.IObservable<TSource>
		static Concat<TSource>(sources: System.IObservable<System.IObservable<TSource>>): System.IObservable<TSource>
		static Concat<TSource>(first: System.IObservable<TSource>, ...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		static Merge<TSource>(sources: any): System.IObservable<TSource>
		static Merge<TSource>(sources: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		static Merge<TSource>(sources: any, maxConcurrent: number): System.IObservable<TSource>
		static Merge<TSource>(sources: any, maxConcurrent: number, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		static Merge<TSource>(...sources: System.IObservable<TSource>[]): System.IObservable<TSource>
		static Merge<TSource>(scheduler: UniRx.IScheduler, ...sources: System.IObservable<TSource>[]): System.IObservable<TSource>
		static Merge<T>(first: System.IObservable<T>, ...seconds: System.IObservable<T>[]): System.IObservable<T>
		static Merge<T>(first: System.IObservable<T>, second: System.IObservable<T>, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Merge<T>(sources: System.IObservable<System.IObservable<T>>): System.IObservable<T>
		static Merge<T>(sources: System.IObservable<System.IObservable<T>>, maxConcurrent: number): System.IObservable<T>
		static Zip<TLeft, TRight, TResult>(left: System.IObservable<TLeft>, right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		static Zip<T>(sources: any): System.IObservable<any>
		static Zip<T>(...sources: System.IObservable<T>[]): System.IObservable<any>
		static Zip<T1, T2, T3, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		static Zip<T1, T2, T3, T4, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		static Zip<T1, T2, T3, T4, T5, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		static Zip<T1, T2, T3, T4, T5, T6, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		static Zip<T1, T2, T3, T4, T5, T6, T7, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		static CombineLatest<TLeft, TRight, TResult>(left: System.IObservable<TLeft>, right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		static CombineLatest<T>(sources: any): System.IObservable<any>
		static CombineLatest<TSource>(...sources: System.IObservable<TSource>[]): System.IObservable<any>
		static CombineLatest<T1, T2, T3, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		static CombineLatest<T1, T2, T3, T4, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		static CombineLatest<T1, T2, T3, T4, T5, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		static CombineLatest<T1, T2, T3, T4, T5, T6, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		static CombineLatest<T1, T2, T3, T4, T5, T6, T7, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		static ZipLatest<TLeft, TRight, TResult>(left: System.IObservable<TLeft>, right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		static ZipLatest<T>(sources: any): System.IObservable<any>
		static ZipLatest<TSource>(...sources: System.IObservable<TSource>[]): System.IObservable<any>
		static ZipLatest<T1, T2, T3, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		static ZipLatest<T1, T2, T3, T4, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		static ZipLatest<T1, T2, T3, T4, T5, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		static ZipLatest<T1, T2, T3, T4, T5, T6, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		static ZipLatest<T1, T2, T3, T4, T5, T6, T7, TR>(source1: System.IObservable<T1>, source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		static Switch<T>(sources: System.IObservable<System.IObservable<T>>): System.IObservable<T>
		static WithLatestFrom<TLeft, TRight, TResult>(left: System.IObservable<TLeft>, right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		static WhenAll<T>(...sources: System.IObservable<T>[]): System.IObservable<T[]>
		static WhenAll(...sources: System.IObservable<UniRx.Unit>[]): System.IObservable<UniRx.Unit>
		static WhenAll<T>(sources: any): System.IObservable<T[]>
		static WhenAll(sources: any): System.IObservable<UniRx.Unit>
		static StartWith<T>(source: System.IObservable<T>, value: T): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, valueFactory: (() => T)): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, ...values: T[]): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, values: any): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler, value: T): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler, values: any): System.IObservable<T>
		static StartWith<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<T>
		static Synchronize<T>(source: System.IObservable<T>): System.IObservable<T>
		static Synchronize<T>(source: System.IObservable<T>, gate: System.Object): System.IObservable<T>
		static ObserveOn<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler): System.IObservable<T>
		static SubscribeOn<T>(source: System.IObservable<T>, scheduler: UniRx.IScheduler): System.IObservable<T>
		static DelaySubscription<T>(source: System.IObservable<T>, dueTime: any): System.IObservable<T>
		static DelaySubscription<T>(source: System.IObservable<T>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static DelaySubscription<T>(source: System.IObservable<T>, dueTime: any): System.IObservable<T>
		static DelaySubscription<T>(source: System.IObservable<T>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Amb<T>(...sources: System.IObservable<T>[]): System.IObservable<T>
		static Amb<T>(sources: any): System.IObservable<T>
		static Amb<T>(source: System.IObservable<T>, second: System.IObservable<T>): System.IObservable<T>
		static AsObservable<T>(source: System.IObservable<T>): System.IObservable<T>
		static ToObservable<T>(source: any): System.IObservable<T>
		static ToObservable<T>(source: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Cast<TSource, TResult>(source: System.IObservable<TSource>): System.IObservable<TResult>
		static Cast<TSource, TResult>(source: System.IObservable<TSource>, witness: TResult): System.IObservable<TResult>
		static OfType<TSource, TResult>(source: System.IObservable<TSource>): System.IObservable<TResult>
		static OfType<TSource, TResult>(source: System.IObservable<TSource>, witness: TResult): System.IObservable<TResult>
		static AsUnitObservable<T>(source: System.IObservable<T>): System.IObservable<UniRx.Unit>
		static AsSingleUnitObservable<T>(source: System.IObservable<T>): System.IObservable<UniRx.Unit>
		static Create<T>(subscribe: ((arg: System.IObserver<T>) => System.IDisposable)): System.IObservable<T>
		static Create<T>(subscribe: ((arg: System.IObserver<T>) => System.IDisposable), isRequiredSubscribeOnCurrentThread: boolean): System.IObservable<T>
		static CreateWithState<T, TState>(state: TState, subscribe: ((arg1: TState, arg2: System.IObserver<T>) => System.IDisposable)): System.IObservable<T>
		static CreateWithState<T, TState>(state: TState, subscribe: ((arg1: TState, arg2: System.IObserver<T>) => System.IDisposable), isRequiredSubscribeOnCurrentThread: boolean): System.IObservable<T>
		static CreateSafe<T>(subscribe: ((arg: System.IObserver<T>) => System.IDisposable)): System.IObservable<T>
		static CreateSafe<T>(subscribe: ((arg: System.IObserver<T>) => System.IDisposable), isRequiredSubscribeOnCurrentThread: boolean): System.IObservable<T>
		static Empty<T>(): System.IObservable<T>
		static Empty<T>(scheduler: UniRx.IScheduler): System.IObservable<T>
		static Empty<T>(witness: T): System.IObservable<T>
		static Empty<T>(scheduler: UniRx.IScheduler, witness: T): System.IObservable<T>
		static Never<T>(): System.IObservable<T>
		static Never<T>(witness: T): System.IObservable<T>
		static Return<T>(value: T): System.IObservable<T>
		static Return<T>(value: T, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Return(value: UniRx.Unit): System.IObservable<UniRx.Unit>
		static Return(value: boolean): System.IObservable<boolean>
		static Return(value: number): System.IObservable<number>
		static ReturnUnit(): System.IObservable<UniRx.Unit>
		static Throw<T>(error: System.Exception): System.IObservable<T>
		static Throw<T>(error: System.Exception, witness: T): System.IObservable<T>
		static Throw<T>(error: System.Exception, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Throw<T>(error: System.Exception, scheduler: UniRx.IScheduler, witness: T): System.IObservable<T>
		static Range(start: number, count: number): System.IObservable<number>
		static Range(start: number, count: number, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Repeat<T>(value: T): System.IObservable<T>
		static Repeat<T>(value: T, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Repeat<T>(value: T, repeatCount: number): System.IObservable<T>
		static Repeat<T>(value: T, repeatCount: number, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Repeat<T>(source: System.IObservable<T>): System.IObservable<T>
		static RepeatSafe<T>(source: System.IObservable<T>): System.IObservable<T>
		static Defer<T>(observableFactory: (() => System.IObservable<T>)): System.IObservable<T>
		static Start<T>(func: (() => T)): System.IObservable<T>
		static Start<T>(func: (() => T), timeSpan: any): System.IObservable<T>
		static Start<T>(func: (() => T), scheduler: UniRx.IScheduler): System.IObservable<T>
		static Start<T>(func: (() => T), timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Start(action: (() => void)): System.IObservable<UniRx.Unit>
		static Start(action: (() => void), timeSpan: any): System.IObservable<UniRx.Unit>
		static Start(action: (() => void), scheduler: UniRx.IScheduler): System.IObservable<UniRx.Unit>
		static Start(action: (() => void), timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Unit>
		static ToAsync<T>(func: (() => T)): (() => System.IObservable<T>)
		static ToAsync<T>(func: (() => T), scheduler: UniRx.IScheduler): (() => System.IObservable<T>)
		static ToAsync(action: (() => void)): (() => System.IObservable<UniRx.Unit>)
		static ToAsync(action: (() => void), scheduler: UniRx.IScheduler): (() => System.IObservable<UniRx.Unit>)
		static Select<T, TR>(source: System.IObservable<T>, selector: ((arg: T) => TR)): System.IObservable<TR>
		static Select<T, TR>(source: System.IObservable<T>, selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		static Where<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static Where<T>(source: System.IObservable<T>, predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		static ContinueWith<T, TR>(source: System.IObservable<T>, other: System.IObservable<TR>): System.IObservable<TR>
		static ContinueWith<T, TR>(source: System.IObservable<T>, selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		static SelectMany<T, TR>(source: System.IObservable<T>, other: System.IObservable<TR>): System.IObservable<TR>
		static SelectMany<T, TR>(source: System.IObservable<T>, selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		static SelectMany<TSource, TResult>(source: System.IObservable<TSource>, selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		static SelectMany<T, TC, TR>(source: System.IObservable<T>, collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		static SelectMany<TSource, TCollection, TResult>(source: System.IObservable<TSource>, collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		static SelectMany<TSource, TResult>(source: System.IObservable<TSource>, selector: ((arg: TSource) => any)): System.IObservable<TResult>
		static SelectMany<TSource, TResult>(source: System.IObservable<TSource>, selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		static SelectMany<TSource, TCollection, TResult>(source: System.IObservable<TSource>, collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		static SelectMany<TSource, TCollection, TResult>(source: System.IObservable<TSource>, collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		static ToArray<T>(source: System.IObservable<T>): System.IObservable<T[]>
		static ToList<T>(source: System.IObservable<T>): System.IObservable<any>
		static Do<T>(source: System.IObservable<T>, observer: System.IObserver<T>): System.IObservable<T>
		static Do<T>(source: System.IObservable<T>, onNext: ((obj: T) => void)): System.IObservable<T>
		static Do<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void)): System.IObservable<T>
		static Do<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onCompleted: (() => void)): System.IObservable<T>
		static Do<T>(source: System.IObservable<T>, onNext: ((obj: T) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<T>
		static DoOnError<T>(source: System.IObservable<T>, onError: ((obj: System.Exception) => void)): System.IObservable<T>
		static DoOnCompleted<T>(source: System.IObservable<T>, onCompleted: (() => void)): System.IObservable<T>
		static DoOnTerminate<T>(source: System.IObservable<T>, onTerminate: (() => void)): System.IObservable<T>
		static DoOnSubscribe<T>(source: System.IObservable<T>, onSubscribe: (() => void)): System.IObservable<T>
		static DoOnCancel<T>(source: System.IObservable<T>, onCancel: (() => void)): System.IObservable<T>
		static Materialize<T>(source: System.IObservable<T>): System.IObservable<UniRx.Notification<T>>
		static Dematerialize<T>(source: System.IObservable<UniRx.Notification<T>>): System.IObservable<T>
		static DefaultIfEmpty<T>(source: System.IObservable<T>): System.IObservable<T>
		static DefaultIfEmpty<T>(source: System.IObservable<T>, defaultValue: T): System.IObservable<T>
		static Distinct<TSource>(source: System.IObservable<TSource>): System.IObservable<TSource>
		static Distinct<TSource>(source: System.IObservable<TSource>, comparer: any): System.IObservable<TSource>
		static Distinct<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		static Distinct<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		static DistinctUntilChanged<T>(source: System.IObservable<T>): System.IObservable<T>
		static DistinctUntilChanged<T>(source: System.IObservable<T>, comparer: any): System.IObservable<T>
		static DistinctUntilChanged<T, TKey>(source: System.IObservable<T>, keySelector: ((arg: T) => TKey)): System.IObservable<T>
		static DistinctUntilChanged<T, TKey>(source: System.IObservable<T>, keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<T>
		static IgnoreElements<T>(source: System.IObservable<T>): System.IObservable<T>
		static ForEachAsync<T>(source: System.IObservable<T>, onNext: ((obj: T) => void)): System.IObservable<UniRx.Unit>
		static ForEachAsync<T>(source: System.IObservable<T>, onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		static Finally<T>(source: System.IObservable<T>, finallyAction: (() => void)): System.IObservable<T>
		static Catch<T, TException>(source: System.IObservable<T>, errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<T>
		static Catch<TSource>(sources: any): System.IObservable<TSource>
		static CatchIgnore<TSource>(source: System.IObservable<TSource>): System.IObservable<TSource>
		static CatchIgnore<TSource, TException>(source: System.IObservable<TSource>, errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		static Retry<TSource>(source: System.IObservable<TSource>): System.IObservable<TSource>
		static Retry<TSource>(source: System.IObservable<TSource>, retryCount: number): System.IObservable<TSource>
		static OnErrorRetry<TSource>(source: System.IObservable<TSource>): System.IObservable<TSource>
		static OnErrorRetry<TSource, TException>(source: System.IObservable<TSource>, onError: ((obj: TException) => void)): System.IObservable<TSource>
		static OnErrorRetry<TSource, TException>(source: System.IObservable<TSource>, onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		static OnErrorRetry<TSource, TException>(source: System.IObservable<TSource>, onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		static OnErrorRetry<TSource, TException>(source: System.IObservable<TSource>, onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		static OnErrorRetry<TSource, TException>(source: System.IObservable<TSource>, onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		static FromEventPattern<TDelegate, TEventArgs>(conversion: ((arg: ((sender: System.Object, e: TEventArgs) => void)) => TDelegate), addHandler: ((obj: TDelegate) => void), removeHandler: ((obj: TDelegate) => void)): System.IObservable<UniRx.EventPattern<TEventArgs>>
		static FromEvent<TDelegate>(conversion: ((arg: (() => void)) => TDelegate), addHandler: ((obj: TDelegate) => void), removeHandler: ((obj: TDelegate) => void)): System.IObservable<UniRx.Unit>
		static FromEvent<TDelegate, TEventArgs>(conversion: ((arg: ((obj: TEventArgs) => void)) => TDelegate), addHandler: ((obj: TDelegate) => void), removeHandler: ((obj: TDelegate) => void)): System.IObservable<TEventArgs>
		static FromEvent(addHandler: ((obj: (() => void)) => void), removeHandler: ((obj: (() => void)) => void)): System.IObservable<UniRx.Unit>
		static FromEvent<T>(addHandler: ((obj: ((obj: T) => void)) => void), removeHandler: ((obj: ((obj: T) => void)) => void)): System.IObservable<T>
		static FromAsyncPattern<TResult>(begin: ((arg1: ((ar: any) => void), arg2: System.Object) => any), end: ((arg: any) => TResult)): (() => System.IObservable<TResult>)
		static FromAsyncPattern<T1, TResult>(begin: ((arg1: T1, arg2: ((ar: any) => void), arg3: System.Object) => any), end: ((arg: any) => TResult)): ((arg: T1) => System.IObservable<TResult>)
		static FromAsyncPattern<T1, T2, TResult>(begin: ((arg1: T1, arg2: T2, arg3: ((ar: any) => void), arg4: System.Object) => any), end: ((arg: any) => TResult)): ((arg1: T1, arg2: T2) => System.IObservable<TResult>)
		static FromAsyncPattern(begin: ((arg1: ((ar: any) => void), arg2: System.Object) => any), end: ((obj: any) => void)): (() => System.IObservable<UniRx.Unit>)
		static FromAsyncPattern<T1>(begin: ((arg1: T1, arg2: ((ar: any) => void), arg3: System.Object) => any), end: ((obj: any) => void)): ((arg: T1) => System.IObservable<UniRx.Unit>)
		static FromAsyncPattern<T1, T2>(begin: ((arg1: T1, arg2: T2, arg3: ((ar: any) => void), arg4: System.Object) => any), end: ((obj: any) => void)): ((arg1: T1, arg2: T2) => System.IObservable<UniRx.Unit>)
		static Take<T>(source: System.IObservable<T>, count: number): System.IObservable<T>
		static Take<T>(source: System.IObservable<T>, duration: any): System.IObservable<T>
		static Take<T>(source: System.IObservable<T>, duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static TakeWhile<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static TakeWhile<T>(source: System.IObservable<T>, predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		static TakeUntil<T, TOther>(source: System.IObservable<T>, other: System.IObservable<TOther>): System.IObservable<T>
		static TakeLast<T>(source: System.IObservable<T>, count: number): System.IObservable<T>
		static TakeLast<T>(source: System.IObservable<T>, duration: any): System.IObservable<T>
		static TakeLast<T>(source: System.IObservable<T>, duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Skip<T>(source: System.IObservable<T>, count: number): System.IObservable<T>
		static Skip<T>(source: System.IObservable<T>, duration: any): System.IObservable<T>
		static Skip<T>(source: System.IObservable<T>, duration: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static SkipWhile<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static SkipWhile<T>(source: System.IObservable<T>, predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<T>
		static SkipUntil<T, TOther>(source: System.IObservable<T>, other: System.IObservable<TOther>): System.IObservable<T>
		static Buffer<T>(source: System.IObservable<T>, count: number): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, count: number, skip: number): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any, count: number): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any, timeShift: any): System.IObservable<any>
		static Buffer<T>(source: System.IObservable<T>, timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		static Buffer<TSource, TWindowBoundary>(source: System.IObservable<TSource>, windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		static Pairwise<T>(source: System.IObservable<T>): System.IObservable<UniRx.Pair<T>>
		static Pairwise<T, TR>(source: System.IObservable<T>, selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		static Last<T>(source: System.IObservable<T>): System.IObservable<T>
		static Last<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static LastOrDefault<T>(source: System.IObservable<T>): System.IObservable<T>
		static LastOrDefault<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static First<T>(source: System.IObservable<T>): System.IObservable<T>
		static First<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static FirstOrDefault<T>(source: System.IObservable<T>): System.IObservable<T>
		static FirstOrDefault<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static Single<T>(source: System.IObservable<T>): System.IObservable<T>
		static Single<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static SingleOrDefault<T>(source: System.IObservable<T>): System.IObservable<T>
		static SingleOrDefault<T>(source: System.IObservable<T>, predicate: ((arg: T) => boolean)): System.IObservable<T>
		static GroupBy<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		static GroupBy<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		static GroupBy<TSource, TKey, TElement>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		static GroupBy<TSource, TKey, TElement>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		static GroupBy<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		static GroupBy<TSource, TKey>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		static GroupBy<TSource, TKey, TElement>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		static GroupBy<TSource, TKey, TElement>(source: System.IObservable<TSource>, keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		static Interval(period: any): System.IObservable<number>
		static Interval(period: any, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Timer(dueTime: any): System.IObservable<number>
		static Timer(dueTime: any): System.IObservable<number>
		static Timer(dueTime: any, period: any): System.IObservable<number>
		static Timer(dueTime: any, period: any): System.IObservable<number>
		static Timer(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Timer(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Timer(dueTime: any, period: any, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Timer(dueTime: any, period: any, scheduler: UniRx.IScheduler): System.IObservable<number>
		static Timestamp<TSource>(source: System.IObservable<TSource>): System.IObservable<UniRx.Timestamped<TSource>>
		static Timestamp<TSource>(source: System.IObservable<TSource>, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		static TimeInterval<TSource>(source: System.IObservable<TSource>): System.IObservable<UniRx.TimeInterval<TSource>>
		static TimeInterval<TSource>(source: System.IObservable<TSource>, scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		static Delay<T>(source: System.IObservable<T>, dueTime: any): System.IObservable<T>
		static Delay<TSource>(source: System.IObservable<TSource>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		static Sample<T>(source: System.IObservable<T>, interval: any): System.IObservable<T>
		static Sample<T>(source: System.IObservable<T>, interval: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Throttle<TSource>(source: System.IObservable<TSource>, dueTime: any): System.IObservable<TSource>
		static Throttle<TSource>(source: System.IObservable<TSource>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		static ThrottleFirst<TSource>(source: System.IObservable<TSource>, dueTime: any): System.IObservable<TSource>
		static ThrottleFirst<TSource>(source: System.IObservable<TSource>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		static Timeout<T>(source: System.IObservable<T>, dueTime: any): System.IObservable<T>
		static Timeout<T>(source: System.IObservable<T>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static Timeout<T>(source: System.IObservable<T>, dueTime: any): System.IObservable<T>
		static Timeout<T>(source: System.IObservable<T>, dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<T>
		static FromCoroutine(coroutine: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		static FromCoroutine(coroutine: ((arg: any) => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		static FromMicroCoroutine(coroutine: (() => any), publishEveryYield: boolean, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		static FromMicroCoroutine(coroutine: ((arg: any) => any), publishEveryYield: boolean, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		static FromCoroutineValue<T>(coroutine: (() => any), nullAsNextUpdate: boolean): System.IObservable<T>
		static FromCoroutineValue<T>(coroutine: ((arg: any) => any), nullAsNextUpdate: boolean): System.IObservable<T>
		static FromCoroutine<T>(coroutine: ((arg: System.IObserver<T>) => any)): System.IObservable<T>
		static FromMicroCoroutine<T>(coroutine: ((arg: System.IObserver<T>) => any), frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static FromCoroutine<T>(coroutine: ((arg1: System.IObserver<T>, arg2: any) => any)): System.IObservable<T>
		static FromMicroCoroutine<T>(coroutine: ((arg1: System.IObserver<T>, arg2: any) => any), frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static SelectMany<T>(source: System.IObservable<T>, coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		static SelectMany<T>(source: System.IObservable<T>, selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		static SelectMany<T>(source: System.IObservable<T>, selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		static ToObservable(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		static ToYieldInstruction(coroutine: any): UniRx.ObservableYieldInstruction<UniRx.Unit>
		static ToYieldInstruction(coroutine: any, throwOnError: boolean): UniRx.ObservableYieldInstruction<UniRx.Unit>
		static ToYieldInstruction(coroutine: any, cancellationToken: any): UniRx.ObservableYieldInstruction<UniRx.Unit>
		static ToYieldInstruction(coroutine: any, throwOnError: boolean, cancellationToken: any): UniRx.ObservableYieldInstruction<UniRx.Unit>
		static EveryUpdate(): System.IObservable<number>
		static EveryFixedUpdate(): System.IObservable<number>
		static EveryEndOfFrame(): System.IObservable<number>
		static EveryGameObjectUpdate(): System.IObservable<number>
		static EveryLateUpdate(): System.IObservable<number>
		static NextFrame(frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		static IntervalFrame(intervalFrameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<number>
		static TimerFrame(dueTimeFrameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<number>
		static TimerFrame(dueTimeFrameCount: number, periodFrameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<number>
		static DelayFrame<T>(source: System.IObservable<T>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static Sample<T, T2>(source: System.IObservable<T>, sampler: System.IObservable<T2>): System.IObservable<T>
		static SampleFrame<T>(source: System.IObservable<T>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static ThrottleFrame<TSource>(source: System.IObservable<TSource>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		static ThrottleFirstFrame<TSource>(source: System.IObservable<TSource>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		static TimeoutFrame<T>(source: System.IObservable<T>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static DelayFrameSubscription<T>(source: System.IObservable<T>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<T>
		static ToYieldInstruction<T>(source: System.IObservable<T>): UniRx.ObservableYieldInstruction<T>
		static ToYieldInstruction<T>(source: System.IObservable<T>, cancel: any): UniRx.ObservableYieldInstruction<T>
		static ToYieldInstruction<T>(source: System.IObservable<T>, throwOnError: boolean): UniRx.ObservableYieldInstruction<T>
		static ToYieldInstruction<T>(source: System.IObservable<T>, throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<T>
		static ToAwaitableEnumerator<T>(source: System.IObservable<T>, cancel: any): any
		static ToAwaitableEnumerator<T>(source: System.IObservable<T>, onResult: ((obj: T) => void), cancel: any): any
		static ToAwaitableEnumerator<T>(source: System.IObservable<T>, onError: ((obj: System.Exception) => void), cancel: any): any
		static ToAwaitableEnumerator<T>(source: System.IObservable<T>, onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		static StartAsCoroutine<T>(source: System.IObservable<T>, cancel: any): any
		static StartAsCoroutine<T>(source: System.IObservable<T>, onResult: ((obj: T) => void), cancel: any): any
		static StartAsCoroutine<T>(source: System.IObservable<T>, onError: ((obj: System.Exception) => void), cancel: any): any
		static StartAsCoroutine<T>(source: System.IObservable<T>, onResult: ((obj: T) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		static ObserveOnMainThread<T>(source: System.IObservable<T>): System.IObservable<T>
		static ObserveOnMainThread<T>(source: System.IObservable<T>, dispatchType: UniRx.MainThreadDispatchType): System.IObservable<T>
		static SubscribeOnMainThread<T>(source: System.IObservable<T>): System.IObservable<T>
		static EveryApplicationPause(): System.IObservable<boolean>
		static EveryApplicationFocus(): System.IObservable<boolean>
		static OnceApplicationQuit(): System.IObservable<UniRx.Unit>
		static TakeUntilDestroy<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static TakeUntilDestroy<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static TakeUntilDisable<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static TakeUntilDisable<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static RepeatUntilDestroy<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static RepeatUntilDestroy<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static RepeatUntilDisable<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static RepeatUntilDisable<T>(source: System.IObservable<T>, target: any): System.IObservable<T>
		static FrameInterval<T>(source: System.IObservable<T>): System.IObservable<UniRx.FrameInterval<T>>
		static FrameTimeInterval<T>(source: System.IObservable<T>, ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		static BatchFrame<T>(source: System.IObservable<T>): System.IObservable<any>
		static BatchFrame<T>(source: System.IObservable<T>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		static BatchFrame(source: System.IObservable<UniRx.Unit>): System.IObservable<UniRx.Unit>
		static BatchFrame(source: System.IObservable<UniRx.Unit>, frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
	}
}
