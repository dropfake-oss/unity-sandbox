// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Notification
declare namespace UniRx {
	abstract class Notification extends System.Object {
		static CreateOnNext<T>(value: T): UniRx.Notification<T>
		static CreateOnError<T>(error: System.Exception): UniRx.Notification<T>
		static CreateOnCompleted<T>(): UniRx.Notification<T>
	}
}
