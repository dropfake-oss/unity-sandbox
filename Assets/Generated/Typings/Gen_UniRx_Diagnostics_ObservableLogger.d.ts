// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Diagnostics.ObservableLogger
declare namespace UniRx.Diagnostics {
	class ObservableLogger extends System.Object implements System.IObservable<UniRx.Diagnostics.LogEntry> {
		static readonly Listener: UniRx.Diagnostics.ObservableLogger
		static RegisterLogger(logger: UniRx.Diagnostics.Logger): ((obj: UniRx.Diagnostics.LogEntry) => void)
		Subscribe(observer: System.IObserver<UniRx.Diagnostics.LogEntry>): System.IDisposable
		Scan(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Scan(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(accumulator: ((arg1: TSource, arg2: TSource) => TSource)): System.IObservable<TSource>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate)): System.IObservable<TAccumulate>
		Aggregate(seed: TAccumulate, accumulator: ((arg1: TAccumulate, arg2: TSource) => TAccumulate), resultSelector: ((arg: TAccumulate) => TResult)): System.IObservable<TResult>
		GetAwaiter(): UniRx.AsyncSubject<TSource>
		GetAwaiter(cancellationToken: any): UniRx.AsyncSubject<TSource>
		Multicast(subject: UniRx.ISubject<UniRx.Diagnostics.LogEntry>): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Publish(): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Publish(initialValue: T): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		PublishLast(): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(bufferSize: number): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(bufferSize: number, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(window: any): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Replay(bufferSize: number, window: any, scheduler: UniRx.IScheduler): UniRx.IConnectableObservable<UniRx.Diagnostics.LogEntry>
		Share(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Wait(): T
		Wait(timeout: any): T
		Concat(): System.IObservable<TSource>
		Concat(...seconds: System.IObservable<TSource>[]): System.IObservable<TSource>
		Merge(...seconds: System.IObservable<T>[]): System.IObservable<UniRx.Diagnostics.LogEntry>
		Merge(second: System.IObservable<UniRx.Diagnostics.LogEntry>, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		Merge(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Merge(maxConcurrent: number): System.IObservable<UniRx.Diagnostics.LogEntry>
		Zip(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		Zip(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		CombineLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		CombineLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.CombineLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		ZipLatest(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, TR>): System.IObservable<TR>
		ZipLatest(source2: System.IObservable<T2>, source3: System.IObservable<T3>, source4: System.IObservable<T4>, source5: System.IObservable<T5>, source6: System.IObservable<T6>, source7: System.IObservable<T7>, resultSelector: UniRx.Operators.ZipLatestFunc<T1, T2, T3, T4, T5, T6, T7, TR>): System.IObservable<TR>
		Switch(): System.IObservable<UniRx.Diagnostics.LogEntry>
		WithLatestFrom(right: System.IObservable<TRight>, selector: ((arg1: TLeft, arg2: TRight) => TResult)): System.IObservable<TResult>
		StartWith(value: T): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(valueFactory: (() => UniRx.Diagnostics.LogEntry)): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(...values: T[]): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(values: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(scheduler: UniRx.IScheduler, value: T): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(scheduler: UniRx.IScheduler, values: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		StartWith(scheduler: UniRx.IScheduler, ...values: T[]): System.IObservable<UniRx.Diagnostics.LogEntry>
		Synchronize(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Synchronize(gate: System.Object): System.IObservable<UniRx.Diagnostics.LogEntry>
		ObserveOn(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		SubscribeOn(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		DelaySubscription(dueTime: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		DelaySubscription(dueTime: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		DelaySubscription(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		Amb(second: System.IObservable<UniRx.Diagnostics.LogEntry>): System.IObservable<UniRx.Diagnostics.LogEntry>
		AsObservable(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Cast(): System.IObservable<TResult>
		Cast(witness: TResult): System.IObservable<TResult>
		OfType(): System.IObservable<TResult>
		OfType(witness: TResult): System.IObservable<TResult>
		AsUnitObservable(): System.IObservable<UniRx.Unit>
		AsSingleUnitObservable(): System.IObservable<UniRx.Unit>
		Repeat(): System.IObservable<UniRx.Diagnostics.LogEntry>
		RepeatSafe(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Select(selector: ((arg: T) => TR)): System.IObservable<TR>
		Select(selector: ((arg1: T, arg2: number) => TR)): System.IObservable<TR>
		Where(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Where(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		ContinueWith(other: System.IObservable<TR>): System.IObservable<TR>
		ContinueWith(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(other: System.IObservable<TR>): System.IObservable<TR>
		SelectMany(selector: ((arg: T) => System.IObservable<TR>)): System.IObservable<TR>
		SelectMany(selector: ((arg1: TSource, arg2: number) => System.IObservable<TResult>)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: T) => System.IObservable<TC>), resultSelector: ((arg1: T, arg2: TC) => TR)): System.IObservable<TR>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => System.IObservable<TCollection>), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		SelectMany(selector: ((arg: TSource) => any)): System.IObservable<TResult>
		SelectMany(selector: ((arg1: TSource, arg2: number) => any)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg: TSource) => any), resultSelector: ((arg1: TSource, arg2: TCollection) => TResult)): System.IObservable<TResult>
		SelectMany(collectionSelector: ((arg1: TSource, arg2: number) => any), resultSelector: ((arg1: TSource, arg2: number, arg3: TCollection, arg4: number) => TResult)): System.IObservable<TResult>
		ToArray(): System.IObservable<T[]>
		ToList(): System.IObservable<any>
		Do(observer: System.IObserver<UniRx.Diagnostics.LogEntry>): System.IObservable<UniRx.Diagnostics.LogEntry>
		Do(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Do(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Do(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onCompleted: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Do(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DoOnError(onError: ((obj: System.Exception) => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DoOnCompleted(onCompleted: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DoOnTerminate(onTerminate: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DoOnSubscribe(onSubscribe: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DoOnCancel(onCancel: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Materialize(): System.IObservable<UniRx.Notification<T>>
		Dematerialize(): System.IObservable<UniRx.Diagnostics.LogEntry>
		DefaultIfEmpty(): System.IObservable<UniRx.Diagnostics.LogEntry>
		DefaultIfEmpty(defaultValue: T): System.IObservable<UniRx.Diagnostics.LogEntry>
		Distinct(): System.IObservable<TSource>
		Distinct(comparer: any): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey)): System.IObservable<TSource>
		Distinct(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<TSource>
		DistinctUntilChanged(): System.IObservable<UniRx.Diagnostics.LogEntry>
		DistinctUntilChanged(comparer: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey)): System.IObservable<UniRx.Diagnostics.LogEntry>
		DistinctUntilChanged(keySelector: ((arg: T) => TKey), comparer: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		IgnoreElements(): System.IObservable<UniRx.Diagnostics.LogEntry>
		ForEachAsync(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void)): System.IObservable<UniRx.Unit>
		ForEachAsync(onNext: ((arg1: T, arg2: number) => void)): System.IObservable<UniRx.Unit>
		Finally(finallyAction: (() => void)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Catch(errorHandler: ((arg: TException) => System.IObservable<T>)): System.IObservable<UniRx.Diagnostics.LogEntry>
		CatchIgnore(): System.IObservable<TSource>
		CatchIgnore(errorAction: ((obj: TException) => void)): System.IObservable<TSource>
		Retry(): System.IObservable<TSource>
		Retry(retryCount: number): System.IObservable<TSource>
		OnErrorRetry(): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void)): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any): System.IObservable<TSource>
		OnErrorRetry(onError: ((obj: TException) => void), retryCount: number, delay: any, delayScheduler: UniRx.IScheduler): System.IObservable<TSource>
		Take(count: number): System.IObservable<UniRx.Diagnostics.LogEntry>
		Take(duration: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Take(duration: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeWhile(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeUntil(other: System.IObservable<TOther>): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeLast(count: number): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeLast(duration: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeLast(duration: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		Skip(count: number): System.IObservable<UniRx.Diagnostics.LogEntry>
		Skip(duration: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Skip(duration: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		SkipWhile(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		SkipWhile(predicate: ((arg1: T, arg2: number) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		SkipUntil(other: System.IObservable<TOther>): System.IObservable<UniRx.Diagnostics.LogEntry>
		Buffer(count: number): System.IObservable<any>
		Buffer(count: number, skip: number): System.IObservable<any>
		Buffer(timeSpan: any): System.IObservable<any>
		Buffer(timeSpan: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, count: number): System.IObservable<any>
		Buffer(timeSpan: any, count: number, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any): System.IObservable<any>
		Buffer(timeSpan: any, timeShift: any, scheduler: UniRx.IScheduler): System.IObservable<any>
		Buffer(windowBoundaries: System.IObservable<TWindowBoundary>): System.IObservable<any>
		Pairwise(): System.IObservable<UniRx.Pair<T>>
		Pairwise(selector: ((arg1: T, arg2: T) => TR)): System.IObservable<TR>
		Last(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Last(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		LastOrDefault(): System.IObservable<UniRx.Diagnostics.LogEntry>
		LastOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		First(): System.IObservable<UniRx.Diagnostics.LogEntry>
		First(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		FirstOrDefault(): System.IObservable<UniRx.Diagnostics.LogEntry>
		FirstOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		Single(): System.IObservable<UniRx.Diagnostics.LogEntry>
		Single(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		SingleOrDefault(): System.IObservable<UniRx.Diagnostics.LogEntry>
		SingleOrDefault(predicate: ((arg: T) => boolean)): System.IObservable<UniRx.Diagnostics.LogEntry>
		GroupBy(keySelector: ((arg: TSource) => TKey)): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement)): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TSource>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		GroupBy(keySelector: ((arg: TSource) => TKey), elementSelector: ((arg: TSource) => TElement), capacity: number, comparer: any): System.IObservable<UniRx.IGroupedObservable<TKey, TElement>>
		Timestamp(): System.IObservable<UniRx.Timestamped<TSource>>
		Timestamp(scheduler: UniRx.IScheduler): System.IObservable<UniRx.Timestamped<TSource>>
		TimeInterval(): System.IObservable<UniRx.TimeInterval<TSource>>
		TimeInterval(scheduler: UniRx.IScheduler): System.IObservable<UniRx.TimeInterval<TSource>>
		Delay(dueTime: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Delay(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Sample(interval: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Sample(interval: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		Throttle(dueTime: any): System.IObservable<TSource>
		Throttle(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		ThrottleFirst(dueTime: any): System.IObservable<TSource>
		ThrottleFirst(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<TSource>
		Timeout(dueTime: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		Timeout(dueTime: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		Timeout(dueTime: any, scheduler: UniRx.IScheduler): System.IObservable<UniRx.Diagnostics.LogEntry>
		SelectMany(coroutine: any, publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: (() => any), publishEveryYield: boolean): System.IObservable<UniRx.Unit>
		SelectMany(selector: ((arg: T) => any)): System.IObservable<UniRx.Unit>
		DelayFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Diagnostics.LogEntry>
		Sample(sampler: System.IObservable<T2>): System.IObservable<UniRx.Diagnostics.LogEntry>
		SampleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Diagnostics.LogEntry>
		ThrottleFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		ThrottleFirstFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<TSource>
		TimeoutFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Diagnostics.LogEntry>
		DelayFrameSubscription(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Diagnostics.LogEntry>
		ToYieldInstruction(): UniRx.ObservableYieldInstruction<UniRx.Diagnostics.LogEntry>
		ToYieldInstruction(cancel: any): UniRx.ObservableYieldInstruction<UniRx.Diagnostics.LogEntry>
		ToYieldInstruction(throwOnError: boolean): UniRx.ObservableYieldInstruction<UniRx.Diagnostics.LogEntry>
		ToYieldInstruction(throwOnError: boolean, cancel: any): UniRx.ObservableYieldInstruction<UniRx.Diagnostics.LogEntry>
		ToAwaitableEnumerator(cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: UniRx.Diagnostics.LogEntry) => void), cancel: any): any
		ToAwaitableEnumerator(onError: ((obj: System.Exception) => void), cancel: any): any
		ToAwaitableEnumerator(onResult: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(cancel: any): any
		StartAsCoroutine(onResult: ((obj: UniRx.Diagnostics.LogEntry) => void), cancel: any): any
		StartAsCoroutine(onError: ((obj: System.Exception) => void), cancel: any): any
		StartAsCoroutine(onResult: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void), cancel: any): any
		ObserveOnMainThread(): System.IObservable<UniRx.Diagnostics.LogEntry>
		ObserveOnMainThread(dispatchType: UniRx.MainThreadDispatchType): System.IObservable<UniRx.Diagnostics.LogEntry>
		SubscribeOnMainThread(): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeUntilDestroy(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeUntilDestroy(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeUntilDisable(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		TakeUntilDisable(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		RepeatUntilDestroy(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		RepeatUntilDestroy(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		RepeatUntilDisable(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		RepeatUntilDisable(target: any): System.IObservable<UniRx.Diagnostics.LogEntry>
		FrameInterval(): System.IObservable<UniRx.FrameInterval<T>>
		FrameTimeInterval(ignoreTimeScale: boolean): System.IObservable<UniRx.TimeInterval<T>>
		BatchFrame(): System.IObservable<any>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<any>
		BatchFrame(): System.IObservable<UniRx.Unit>
		BatchFrame(frameCount: number, frameCountType: UniRx.FrameCountType): System.IObservable<UniRx.Unit>
		Subscribe(): System.IDisposable
		Subscribe(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void)): System.IDisposable
		Subscribe(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void)): System.IDisposable
		Subscribe(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onCompleted: (() => void)): System.IDisposable
		Subscribe(onNext: ((obj: UniRx.Diagnostics.LogEntry) => void), onError: ((obj: System.Exception) => void), onCompleted: (() => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState(state: TState, onNext: ((arg1: T, arg2: TState) => void), onError: ((arg1: System.Exception, arg2: TState) => void), onCompleted: ((obj: TState) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState2(state1: TState1, state2: TState2, onNext: ((arg1: T, arg2: TState1, arg3: TState2) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2) => void), onCompleted: ((arg1: TState1, arg2: TState2) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		SubscribeWithState3(state1: TState1, state2: TState2, state3: TState3, onNext: ((arg1: T, arg2: TState1, arg3: TState2, arg4: TState3) => void), onError: ((arg1: System.Exception, arg2: TState1, arg3: TState2, arg4: TState3) => void), onCompleted: ((arg1: TState1, arg2: TState2, arg3: TState3) => void)): System.IDisposable
		IsRequiredSubscribeOnCurrentThread(): boolean
		IsRequiredSubscribeOnCurrentThread(scheduler: UniRx.IScheduler): boolean
		ToTask(): any
		ToTask(state: System.Object): any
		ToTask(cancellationToken: any): any
		ToTask(cancellationToken: any, state: System.Object): any
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand
		ToReactiveCommand(initialValue: boolean): UniRx.ReactiveCommand<UniRx.Diagnostics.LogEntry>
		BindToButtonOnClick(button: any, onClick: ((obj: UniRx.Unit) => void), initialValue: boolean): System.IDisposable
		ToReactiveProperty(): UniRx.IReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		ToReactiveProperty(initialValue: T): UniRx.IReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		ToReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		ToSequentialReadOnlyReactiveProperty(): UniRx.ReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		ToReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		ToSequentialReadOnlyReactiveProperty(initialValue: T): UniRx.ReadOnlyReactiveProperty<UniRx.Diagnostics.LogEntry>
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any): System.IDisposable
		SubscribeToText(text: any, selector: ((arg: T) => string)): System.IDisposable
		SubscribeToInteractable(selectable: any): System.IDisposable
		LogToUnityDebug(): System.IDisposable
		Debug(label: string): System.IObservable<UniRx.Diagnostics.LogEntry>
		Debug(logger: UniRx.Diagnostics.Logger): System.IObservable<UniRx.Diagnostics.LogEntry>
	}
}
