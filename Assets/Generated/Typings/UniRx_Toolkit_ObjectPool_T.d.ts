// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Toolkit.ObjectPool`1
declare namespace UniRx.Toolkit {
	abstract class ObjectPool<T> extends System.Object implements System.IDisposable {
		readonly Count: number
		Rent(): T
		Return(instance: T): void
		Clear(callOnBeforeRent: boolean): void
		Shrink(instanceCountRatio: number, minSize: number, callOnBeforeRent: boolean): void
		StartShrinkTimer(checkInterval: any, instanceCountRatio: number, minSize: number, callOnBeforeRent: boolean): System.IDisposable
		PreloadAsync(preloadCount: number, threshold: number): System.IObservable<UniRx.Unit>
		Dispose(): void
	}
}
