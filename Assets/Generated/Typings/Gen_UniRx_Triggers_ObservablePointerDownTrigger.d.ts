// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.Triggers.ObservablePointerDownTrigger
declare namespace UniRx.Triggers {
	class ObservablePointerDownTrigger extends UniRx.Triggers.ObservableTriggerBase {
		constructor()
		OnPointerDownAsObservable(): System.IObservable<any>
	}
}
