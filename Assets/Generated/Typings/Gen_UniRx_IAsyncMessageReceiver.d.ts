// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.IAsyncMessageReceiver
declare namespace UniRx {
	interface IAsyncMessageReceiver {
		Subscribe<T>(asyncMessageReceiver: ((arg: T) => System.IObservable<UniRx.Unit>)): System.IDisposable
	}
}
