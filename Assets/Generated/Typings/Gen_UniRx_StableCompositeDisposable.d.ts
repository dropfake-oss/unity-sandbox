// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.StableCompositeDisposable
declare namespace UniRx {
	abstract class StableCompositeDisposable extends System.Object implements UniRx.ICancelable, System.IDisposable {
		readonly IsDisposed: boolean
		static Create(disposable1: System.IDisposable, disposable2: System.IDisposable): UniRx.ICancelable
		static Create(disposable1: System.IDisposable, disposable2: System.IDisposable, disposable3: System.IDisposable): UniRx.ICancelable
		static Create(disposable1: System.IDisposable, disposable2: System.IDisposable, disposable3: System.IDisposable, disposable4: System.IDisposable): UniRx.ICancelable
		static Create(...disposables: System.IDisposable[]): UniRx.ICancelable
		static CreateUnsafe(disposables: System.IDisposable[]): UniRx.ICancelable
		static Create(disposables: any): UniRx.ICancelable
		Dispose(): void
	}
}
