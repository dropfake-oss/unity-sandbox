// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: UniRx
// Type: UniRx.ObservableWWW
declare namespace UniRx {
	abstract class ObservableWWW extends System.Object {
		static Get(url: string, headers: any, progress: any): System.IObservable<string>
		static GetAndGetBytes(url: string, headers: any, progress: any): System.IObservable<Buffer>
		static GetWWW(url: string, headers: any, progress: any): System.IObservable<any>
		static Post(url: string, postData: Buffer, progress: any): System.IObservable<string>
		static Post(url: string, postData: Buffer, headers: any, progress: any): System.IObservable<string>
		static Post(url: string, content: any, progress: any): System.IObservable<string>
		static Post(url: string, content: any, headers: any, progress: any): System.IObservable<string>
		static PostAndGetBytes(url: string, postData: Buffer, progress: any): System.IObservable<Buffer>
		static PostAndGetBytes(url: string, postData: Buffer, headers: any, progress: any): System.IObservable<Buffer>
		static PostAndGetBytes(url: string, content: any, progress: any): System.IObservable<Buffer>
		static PostAndGetBytes(url: string, content: any, headers: any, progress: any): System.IObservable<Buffer>
		static PostWWW(url: string, postData: Buffer, progress: any): System.IObservable<any>
		static PostWWW(url: string, postData: Buffer, headers: any, progress: any): System.IObservable<any>
		static PostWWW(url: string, content: any, progress: any): System.IObservable<any>
		static PostWWW(url: string, content: any, headers: any, progress: any): System.IObservable<any>
		static LoadFromCacheOrDownload(url: string, version: number, progress: any): System.IObservable<any>
		static LoadFromCacheOrDownload(url: string, version: number, crc: number, progress: any): System.IObservable<any>
		static LoadFromCacheOrDownload(url: string, hash128: any, progress: any): System.IObservable<any>
		static LoadFromCacheOrDownload(url: string, hash128: any, crc: number, progress: any): System.IObservable<any>
	}
}
