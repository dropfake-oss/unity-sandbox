using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugScreen : MonoBehaviour
{
	struct DropDownState
	{
		public bool show;
		public Vector2 scroll;
	}

	protected GUIStyle button;
	protected GUIStyle dropButton;
	protected GUIStyle label;
	protected Rect rect;
	protected GUIStyle labelFixed;
	protected GUIStyle buttonFixed;
	protected GUIStyle textField;

	private float labelSpace;
	private float dropSpace;

	private Dictionary<string, DropDownState> _dropDowns = new Dictionary<string, DropDownState>();

	private void OnGUI()
	{
		button = new GUIStyle(GUI.skin.button);
		label = new GUIStyle(GUI.skin.label);
		labelFixed = new GUIStyle(GUI.skin.label);
		dropButton = new GUIStyle(GUI.skin.button);
		buttonFixed = new GUIStyle(GUI.skin.button);
		textField = new GUIStyle(GUI.skin.textField);

		var width = Screen.width;
		var height = Screen.height;
		var padding = (int)(width * 0.1f);

		var totalWidth = width - 2 * padding;
		labelSpace = totalWidth * 0.25f;

		var fontSize = (int)(height * 0.03f);
		label.fontSize = fontSize;
		button.fontSize = fontSize;
		button.fixedWidth = totalWidth;

		buttonFixed.fontSize = fontSize;
		buttonFixed.fixedWidth = totalWidth - labelSpace;

		dropSpace = totalWidth * 0.1f;

		dropButton.fontSize = fontSize;
		dropButton.fixedWidth = totalWidth - dropSpace - labelSpace;

		labelFixed.fontSize = fontSize;
		labelFixed.fixedWidth = labelSpace - 10;

		textField.fontSize = fontSize;
		textField.fixedWidth = totalWidth - labelSpace;

		rect = new Rect(padding, padding, totalWidth, height - 2 * padding);
		GUILayout.BeginArea(rect);

		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		OnDebugGUI();

		GUILayout.FlexibleSpace();
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("(C) 2021 Drop Fake Inc.");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical();

		GUILayout.EndArea();
	}

	protected void Space()
	{
		Label(string.Empty);
	}

	protected void Label(string name)
	{
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(name, this.label);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}

	protected void Label(string name, string value)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(name, this.labelFixed);
		GUILayout.Label(value, this.label);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}

	protected bool Button(string name)
	{
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		var res = GUILayout.Button(name, button);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		return res;
	}

	protected string TextField(string text, string labelName)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(labelName, labelFixed);
		var res = GUILayout.TextField(text, this.textField);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		return res;
	}

	protected int DropDown(string name, int current, string[] values)
	{
		DropDownState state;
		_dropDowns.TryGetValue(name, out state);

		var currentVal = "Select";

		if (current >= 0 && current <= values.Length) {
			currentVal = values[current];
		}
		
		GUILayout.BeginHorizontal();
		GUILayout.Label(name+":", labelFixed);

		if (GUILayout.Button(currentVal, this.buttonFixed, GUILayout.ExpandWidth(true)))
		{
			state.show = !state.show;
		}

		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		if (state.show)
		{
			state.scroll = GUILayout.BeginScrollView(state.scroll);

			for (int i = 0; i < values.Length; ++i)
			{
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				if (GUILayout.Button(values[i], dropButton))
				{
					current = i;
					state.show = false;
				}

				
				GUILayout.EndHorizontal();
			}

			GUILayout.EndScrollView();
		}

		_dropDowns[name] = state;

		return current;
	}

	protected virtual void OnDebugGUI() {

	}
}
