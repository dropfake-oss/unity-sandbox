using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Selector : DebugScreen
{
	private string[] Scenes;
	private int SceneIndex = 0;

	// Start is called before the first frame update
	void Start()
	{
		int numScenes = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;

		var current = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

		var numSelectableScenes = numScenes;
		if (current.buildIndex >= 0)
		{
			numSelectableScenes--;
		}

		this.Scenes = new string[numSelectableScenes];
		for (int i = 0; i < numScenes; i++)
		{
			var path = System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i));
			if (i != current.buildIndex)
			{
				this.Scenes[numSelectableScenes - 1] = path;
				numSelectableScenes--;
			}
		}

		var scene = PlayerPrefs.GetString("last_scene");
		this.SceneIndex = System.Math.Max(0, System.Array.IndexOf(this.Scenes, scene));
	}

	protected override void OnDebugGUI()
	{
		Label("Scene Selection");

		this.SceneIndex = DropDown("Scene", this.SceneIndex, this.Scenes);

		if (Button("Start"))
		{
			var scene = this.Scenes[this.SceneIndex];
			PlayerPrefs.SetString("last_scene", scene);
			PlayerPrefs.Save();
			UnityEngine.SceneManagement.SceneManager.LoadScene(scene, UnityEngine.SceneManagement.LoadSceneMode.Single);
		}
	}

}

