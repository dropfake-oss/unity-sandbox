using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using Javascript.Core;

#if UNITY_EDITOR
[UnityEditor.InitializeOnLoad]
#endif
public class SetupTypescriptCodegen : MonoBehaviour
{
#if UNITY_EDITOR

	// TPC: TODO: the following should likely all be registered as part of a config - i.e. if user/caller enables observable/reactive
	private static void RegisterObservableClasses()
	{
		Javascript.Unity.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IDisposable));
		Javascript.Unity.DotNetToTypescriptDeclaration.RegisterType(typeof(System.Exception));
		// IObservable has dependency on IObserver - so we need to codegen IObserver in order to codegen IObservable
		Javascript.Unity.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IObserver<>));
		Javascript.Unity.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IObservable<>));
	}

	static SetupTypescriptCodegen()
	{
		Javascript.Unity.DotNetToTypescriptDeclaration.CodeGenAssembliesCallback = GetAssemblies;

		RegisterObservableClasses();

		// Talbot uses List<string> in his Redux Observable - so if you don't want the type to show up as any - then you need to register List<string>
		// i.e. this:
		// Subscribe(observer: System.IObserver<any, string, string>): System.IDisposable
		Javascript.Unity.DotNetToTypescriptDeclaration.RegisterType(typeof(List<string>));

	}
#endif

	public static IReadOnlyList<AssemblyNamespaces> GetAssemblyNamespaces()
	{
		var fixedPointAssembly = (typeof(FixedPointMath.Fixed)).Assembly;
		var fixedPointAssembluyNamespaces = new AssemblyNamespaces(fixedPointAssembly, new string[] { "FixedPointMath" });

		// TPC: to get the extension methods from UniRx - that package just needs to be included in the project -
		// you don't actually need to feed that assembly in for codegen
		// But, feeding UniRx into our codegen is useful for testing our ability to generate typescript declaration files
		var unirxAssembly = (typeof(UniRx.Observable)).Assembly;
		var unirxAssemblyNamespaces = new AssemblyNamespaces(unirxAssembly, new string[] { "UniRx" });

		var assemblyNamespaces = new AssemblyNamespaces[] {
			fixedPointAssembluyNamespaces,
			unirxAssemblyNamespaces
		};
		return assemblyNamespaces;
	}

	static IReadOnlyList<Assembly> GetAssemblies()
	{
		List<Assembly> assemblies = new List<Assembly>();
		var assemblyNamespacesList = GetAssemblyNamespaces();
		foreach (var assemblyNamespaces in assemblyNamespacesList)
		{
			assemblies.Add(assemblyNamespaces.Assembly);
		}
		return assemblies;
	}
}

