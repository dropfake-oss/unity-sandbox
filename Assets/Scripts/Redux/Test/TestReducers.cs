using System.Collections.Generic;
using SuccincT.Functional;
using UniRedux;
using static UniRedux.Reducers;

public static class TestReducers
{
	public static IEnumerable<On<TestState>> CreateReducers()
	{
		var reducers = new List<On<TestState>>
		{
			On<SetTestUint, TestState>(
					(state, action) => state.With( new { TestUint = action.TestUint })
			),

			On<SetTestInt, TestState>(
					(state, action) => state.With( new { TestInt = action.TestInt })
			),

			On<SetTestLong, TestState>(
					(state, action) => state.With( new { TestLong = action.TestLong })
			),

			On<SetTestFloat, TestState>(
					(state, action) => state.With( new { TestFloat = action.TestFloat })
			),

			On<SetTestList, TestState>(
					(state, action) => state.With( new { TestList = action.TestList })
			),

			On<SetNestedObjectiveList, TestState>(
					(state, action) => state.With( new { NestedObjectiveList = action.ObjectiveList})
			),

			On<SetNullableObjective, TestState>(
					(state, action) => state.With( new { NullableObjective = action.Objective})
			),

			On<SetTestLongList, TestState>(
					(state, action) => state.With( new { TestLongList = action.TestList })
			),
		};

		reducers.AddRange(TestSelectors.TestEntitiesConnector.CreateReducers());
		return reducers;
	}
}
