using System;
using System.Collections.Generic;
using UniRedux;

/**
	* Action classes should extend from ReduxAction<ClassName> so that they can be processed by the Action Emitter.
	* Doing so will require them to have a version of the contructor that takes no parameters.
*/

public class SetTestUint : ReduxAction<SetTestUint>
{
	public uint? TestUint;

	public SetTestUint(uint? tu)
	{
		TestUint = tu;
	}

	public SetTestUint() { }
}

public class SetTestInt : ReduxAction<SetTestInt>
{
	public int? TestInt;

	public SetTestInt(int? ti)
	{
		TestInt = ti;
	}

	public SetTestInt() { }
}

public class SetTestLong : ReduxAction<SetTestLong>
{
	public long? TestLong;

	public SetTestLong(long? tl)
	{
		TestLong = tl;
	}

	public SetTestLong() { }
}

public class SetTestFloat : ReduxAction<SetTestFloat>
{
	public float TestFloat;

	public SetTestFloat(float tf)
	{
		TestFloat = tf;
	}

	public SetTestFloat() { }
}

public class SetNestedObjectiveList : ReduxAction<SetNestedObjectiveList>
{
	public List<NestedObjective> ObjectiveList;

	public SetNestedObjectiveList(List<NestedObjective> objective)
	{
		ObjectiveList = objective;
	}

	public SetNestedObjectiveList() { }
}

public class SetTestList : ReduxAction<SetTestList>
{
	public List<PlayerPosition?> TestList;

	public SetTestList(List<PlayerPosition?> testList)
	{
		TestList = testList;
	}

	public SetTestList() { }
}

public class SetNullableObjective : ReduxAction<SetNullableObjective>
{
	public Objective Objective;

	public SetNullableObjective(Objective objective)
	{
		Objective = objective;
	}

	public SetNullableObjective() { }
}

public class SetTestLongList : ReduxAction<SetTestLongList>
{
	public List<TestMcGuffin> TestList;

	public SetTestLongList(List<TestMcGuffin> testList)
	{
		TestList = testList;
	}

	public SetTestLongList() { }
}
