using System.Collections.Generic;
using UniRedux.Entity;

public class Objective
{
	public string Name { get; set; } = "";
	public bool IsCompleted { get; set; }

	public Objective(string name, bool isComplete)
	{
		Name = name;
		IsCompleted = isComplete;
	}

	public Objective() { }
}

public enum PlayerPosition
{
	Left,
	Right
}

public class NestedObjective
{
	public string Name { get; set; } = "";
	public bool? IsCompleted { get; set; } = null;
	public Objective Obj { get; set; }

	public NestedObjective(string name, bool isComplete)
	{
		Name = name;
		IsCompleted = isComplete;
	}

	public NestedObjective() { }
}

public class TestMcGuffin
{
	public string Name { get; set; } = "";
	public long TestLong { get; set; }

	public TestMcGuffin(string name, long num)
	{
		Name = name;
		TestLong = num;
	}

	public TestMcGuffin() { }
}

public class TestState
{
	public List<PlayerPosition?> TestList { get; set; } = null;
	public uint? TestUint { get; set; } = null;
	public int? TestInt { get; set; } = null;
	public long? TestLong { get; set; } = null;
	public float TestFloat { get; set; }
	public List<NestedObjective> NestedObjectiveList { get; set; }
	public Objective NullableObjective { get; set; } = null;
	public List<TestMcGuffin> TestLongList { get; set; } = null;
	public TestEntities<TestState>.EntitiesState TestEntities { get; set; } = null!;

	public static TestState InitialState =>
		new TestState
		{
			TestUint = null,
			TestInt = null,
			TestLong = null,
			TestFloat = 2.1f,
			TestList = null,
			NestedObjectiveList = null,
			NullableObjective = null,
			TestLongList = null,
			TestEntities = new TestEntities<TestState>.EntitiesState()
		};
}

