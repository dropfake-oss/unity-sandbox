using System;
using System.Collections.Generic;
using UniRedux;
using UniRedux.Entity;
using UniRx;

public interface ITestEntity : IWithChanges<ITestEntity>
{
	string ID { get; }
	string Name { get; }

	public long Quantity { get; }
	public IObservable<long> QuantityAsObservable { get; }
}

public class TestEntity : ITestEntity
{
	public string ID { get; set; }
	public string Name { get; set; }

	private readonly MainThreadBehaviorSubject<long> QuantitySubject = new MainThreadBehaviorSubject<long>(0);
	public long Quantity => this.QuantitySubject.Value;
	public IObservable<long> QuantityAsObservable => this.QuantitySubject.AsObservable();

	public void Increment()
	{
		this.QuantitySubject.OnNext(this.Quantity + 1);
	}

	public void Decrement()
	{
		if (this.Quantity > 0)
		{
			this.QuantitySubject.OnNext(this.Quantity - 1);
		}
	}

	public override string ToString()
	{
		return string.Format("{0} - {1}: {2}", this.ID, this.Name, this.Quantity);
	}

	public IObservable<ITestEntity> Changed => this.QuantityAsObservable.Select(q => this);

	public static bool HasSome(ITestEntity? entity) => entity?.Quantity > 0;
}

public class TestEntities<TRootState> : StandardEntityConnector<TRootState, ITestEntity> where TRootState : class, new()
{
	private TestEntities(Func<TRootState, EntitiesState> getEntitiesStateFromContainer, Func<TRootState, EntitiesState, TRootState> setEntitiesStateToContainer)
		:
	base(getEntitiesStateFromContainer, setEntitiesStateToContainer, (ITestEntity e) => e.ID)
	{ }



	public ISelectorWithProps<TRootState, string?, ITestEntity?> SelectAEntity => this.SelectEntityById;
	public ISelectorWithProps<TRootState, IEnumerable<string?>?, List<ITestEntity?>> SelectTheseTestEntities => this.SelectEntitiesByIds;

	public static TestEntities<TRootState> Create(Func<TRootState, EntitiesState> getEntitiesStateFromContainer, Func<TRootState, EntitiesState, TRootState> setEntitiesStateToContainer)
	{
		return new TestEntities<TRootState>(getEntitiesStateFromContainer, setEntitiesStateToContainer);
	}
}
