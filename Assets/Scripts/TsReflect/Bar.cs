using System;

using Javascript.Codegen.Dts;

namespace Foo
{
	[GenerateType]
	public class Bar
	{
		public Bar()
		{
		}

		public object GetData()
		{
			return new object();
		}
	}
}

