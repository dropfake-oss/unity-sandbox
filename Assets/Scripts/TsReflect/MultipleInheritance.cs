using System;

namespace Hooligan.Foo
{
	[Javascript.Codegen.Dts.GenerateType]
	public interface IBehaviour
	{
		public string Name { get; }
	}
}

namespace Rabbit.Fudge
{
	[Javascript.Codegen.Dts.GenerateType]
	public interface IEffect : Hooligan.Foo.IBehaviour
	{

	}
}

namespace Lord.Dance
{
	[Javascript.Codegen.Dts.GenerateType]
	public interface IAction : Hooligan.Foo.IBehaviour, Rabbit.Fudge.IEffect
	{

	}
}

namespace Concrete.Stuff
{
	[Javascript.Codegen.Dts.GenerateType]
	public class Boom : Hooligan.Foo.IBehaviour, Rabbit.Fudge.IEffect, Lord.Dance.IAction
	{
		public string Name { get; }

	}
}