using System;
using System.Collections.Generic;

using Javascript.Codegen.Dts;
using UniRx;

namespace Typescript.Gen.Test
{

	[GenerateType]
	public class NameObserver : IObserver<string>
	{

		private string _last;

		private IDisposable unsubscriber;

		public virtual void Subscribe(IObservable<string> provider)
		{
			if (provider != null)
				unsubscriber = provider.Subscribe(this);
		}
		public virtual void Unsubscribe()
		{
			unsubscriber.Dispose();
		}

		// TPC: interesting - the following compiles in C# - but does not get properly code-gen'ed into typescript
		//void IObserver<string>.OnCompleted()
		public void OnCompleted()
		{
			UnityEngine.Debug.Log("NameObserver: OnCompleted: _last: " + _last);
		}

		//void IObserver<string>.OnError(Exception error)
		public void OnError(Exception error)
		{
			UnityEngine.Debug.LogError("NameObserver: OnError: error: " + error);
		}

		//void IObserver<string>.OnNext(string value)
		public void OnNext(string value)
		{
			_last = value;
			UnityEngine.Debug.Log("NameObserver: OnNext: value: " + value);
		}

	}

	[GenerateType]
	public class NameObservable : IObservable<string>
	{
		private List<IObserver<string>> observers = new List<IObserver<string>>();

		public IDisposable Subscribe(IObserver<string> observer)
		{
			if (! observers.Contains(observer))
				observers.Add(observer);
			return new Unsubscriber(observers, observer);
		}

		private class Unsubscriber : IDisposable
		{
			private List<IObserver<string>> _observers;
			private IObserver<string> _observer;

			public Unsubscriber(List<IObserver<string>> observers, IObserver<string> observer)
			{
				this._observers = observers;
				this._observer = observer;
			}

			public void Dispose()
			{
				if (_observer != null && _observers.Contains(_observer))
					_observers.Remove(_observer);
			}
		}

		public void UpdateName(string name)
		{
			foreach (var observer in observers)
			{
				observer.OnNext(name);
			}
		}

		public void CommitName()
		{
			foreach (var observer in observers.ToArray())
			{
				observer.OnCompleted();
			}

			observers.Clear();
		}
	}

	[GenerateType]
	public class FancyObservable : IObservable<(List<string> entities, string cmd, string data)>
	{
		private List<IObserver<(List<string> entities, string cmd, string data)>> observers = new List<IObserver<(List<string> entities, string cmd, string data)>>();

		public IDisposable Subscribe(IObserver<(List<string> entities, string cmd, string data)> observer)
		{
			if (!observers.Contains(observer))
				observers.Add(observer);
			return new Unsubscriber(observers, observer);
		}

		private class Unsubscriber : IDisposable
		{
			private List<IObserver<(List<string> entities, string cmd, string data)>> _observers;
			private IObserver<(List<string> entities, string cmd, string data)> _observer;

			public Unsubscriber(List<IObserver<(List<string> entities, string cmd, string data)>> observers, IObserver<(List<string> entities, string cmd, string data)> observer)
			{
				this._observers = observers;
				this._observer = observer;
			}

			public void Dispose()
			{
				if (_observer != null && _observers.Contains(_observer))
					_observers.Remove(_observer);
			}
		}

		public void Update(List<string> entities, string cmd, string data)
		{
			foreach (var observer in observers)
			{
				observer.OnNext((entities, cmd, data));
			}
		}

		public void Commit()
		{
			foreach (var observer in observers.ToArray())
			{
				observer.OnCompleted();
			}

			observers.Clear();
		}
	}


	[GenerateType]
	public class ObservableTest
	{

		/*IObservable<(List<string> entities, string cmd, string data)> OnSelectedEntityCommand
		{
			get
			{

			}
		}
		*/

		public ObservableTest()
		{
		}

		public void SimpleObservableTest()
		{
			NameObservable nameObservable = new NameObservable();
			NameObserver nameObserver = new NameObserver();
			nameObserver.Subscribe(nameObservable);

			nameObservable.Subscribe((updatedName) =>
			{
				UnityEngine.Debug.Log("SimpleObservableTest: UniRx: Subscribe: updatedName: " + updatedName);
			});

			nameObservable.UpdateName("testing");
			nameObservable.CommitName();
		}

		public void FancyObservableTest()
		{
			FancyObservable fancyObservable = new FancyObservable();
			fancyObservable.Subscribe((updatedData) =>
			{
				UnityEngine.Debug.Log("FancyObservableTest: UniRx: Subscribe: updatedData: " + updatedData.entities.Count + " and: " + updatedData.cmd + " and: " + updatedData.data);
			});

			var list = new List<string>() { "tut" };
			fancyObservable.Update(list, "testing", "deadbeef");
			fancyObservable.Commit();
		}

		public object GetData()
		{
			return new object();
		}
	}
}

