using DebugMenu;
using BehaviourTree;
using Link.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UI.Core;
using UI.Tweening;
using UniRedux;
using UniRx;
using UnityEngine;
using UnityWebView;
using Builds.Unity;

using static TestSelectors;
using Container.Utils;
using UniRedux.Entity;

public class SampleBehaviourContext
{
	public int Foo = 0;
	public string Bar = "";
}

public class SampleBehaviour : BaseBehaviour<SampleBehaviourContext>
{
	SampleBehaviour() : base("sample")
	{
	}

	protected override BehaviourStatus Update(SampleBehaviourContext context)
	{
		return BehaviourStatus.Succeeded;
	}
}

public interface ISampleNode : IWithChanges<ISampleNode>
{
	public string ID { get; }

	bool IsLocked { get; }
	IObservable<bool> IsLockedAsObservable { get; }

	bool IsComplete { get; }
	IObservable<bool> IsCompleteAsObservable { get; }
}

public class SampleNode : ISampleNode
{
	public string ID { get; private set; }

	public readonly BehaviorSubject<bool> IsLockedSubject;
	public bool IsLocked => this.IsLockedSubject.Value;
	public IObservable<bool> IsLockedAsObservable => this.IsLockedSubject;

	public readonly BehaviorSubject<bool> IsCompleteSubject;
	public bool IsComplete => this.IsCompleteSubject.Value;
	public IObservable<bool> IsCompleteAsObservable => this.IsCompleteSubject;

	public SampleNode(string id, bool complete = false, bool locked = false)
	{
		this.ID = id;
		this.IsLockedSubject = new BehaviorSubject<bool>(locked);
		this.IsCompleteSubject = new BehaviorSubject<bool>(complete);
	}

	public IObservable<ISampleNode> Changed => Observable.CombineLatest<bool, bool, ISampleNode>(this.IsLockedSubject, this.IsCompleteSubject, (locked, complete) => this);
}

public class Sampler : MonoBehaviourWithSubscriptions
{
	private ExampleDebugMenu ExampleDebugMenu = null!;
	private StandardEntityConnectorDebugMenuWithChanges<TestState, ITestEntity> StandardEntityConnectorDebugMenu = null!;
	private StandardEntityConnectorDebugMenuWithChanges<TestState, ITestEntity> AltnernateStandardEntityConnectorDebugMenu = null!;

	private ScriptingBackend _scriptingBackend;

	public ReduxStore<TestState> TestStore;
	private Subject<IReadOnlyList<TestEntity>> ReloadedTestEntities = new Subject<IReadOnlyList<TestEntity>>();

	public BehaviorSubject<CloudBuildInfo?> BuildInfo = new BehaviorSubject<CloudBuildInfo?>(null);
	public IObservable<string?> BuildNumber => this.BuildInfo.Select(info => info?.BuildNumber.ToString()).ObserveOnMainThread();
	public IObservable<string?> Branch => this.BuildInfo.Select(info => info?.Branch).ObserveOnMainThread();
	public IObservable<string?> Commit => this.BuildInfo.Select(info => info?.CommitSHA).ObserveOnMainThread();

	WebViewObject webViewObject;

	private void Awake()
	{
		SetupReduxStore();
	}

	void Start()
	{
		Task.WaitAny(FetchBuildInfo(), Task.Delay(2000));

		this.ExampleDebugMenu = ExampleDebugMenu.AddExampleMenu(DebugMenu.DebugMenuRoot.Instance);

		Action<ITestEntity> onIncrement = (entity) => (entity as TestEntity)?.Increment();
		var incrementOp = ("Increment", onIncrement);

		Action<ITestEntity> onDecrement = (entity) => (entity as TestEntity)?.Decrement();
		var decrementOp = ("Decrement", onDecrement);

		Func<IEnumerable<ITestEntity>> onReload = () =>
		{
			return new[]
				{
					new TestEntity{ ID = "hello", Name = "Hello World" },
					new TestEntity{ ID = "foo", Name = "Foo Bar" },
				};
		};

		this.StandardEntityConnectorDebugMenu = new StandardEntityConnectorDebugMenuWithChanges<TestState, ITestEntity>(DebugMenu.DebugMenuRoot.Instance, "TestEntities", this.TestStore, TestEntitiesConnector, onReload, incrementOp, decrementOp);
		this.AltnernateStandardEntityConnectorDebugMenu = new StandardEntityConnectorDebugMenuWithChanges<TestState, ITestEntity>(DebugMenu.DebugMenuRoot.Instance, "TestEntities No Reload", this.TestStore, TestEntitiesConnector, incrementOp, decrementOp);

		var sub = this.TestStore.Select(TestEntitiesConnector.SelectAEntity, null)
			.First()
			.Subscribe(e =>
			{
				UnityEngine.Debug.Log("Testing SelectAEntity with null: " + e);
			});
		this.Subs.Add(sub);

		sub = this.TestStore.Select(TestEntitiesConnector.SelectAEntity, "foo")
			.IncludeChanges()
			.Subscribe(e =>
			{
				UnityEngine.Debug.Log("Testing IncludeChanges with 'foo': " + e);
			});
		this.Subs.Add(sub);

		sub = this.TestStore.Select(TestEntitiesConnector.SelectTheseTestEntities, null)
			.First()
			.Subscribe(es =>
			{
				UnityEngine.Debug.Log("Testing SelectThese with null: " + String.Join(", ", es.Select(e => e?.ID)));
			});
		this.Subs.Add(sub);

		sub = this.TestStore.Select(TestEntitiesConnector.SelectTheseTestEntities, new string[] { "foo", "bar", null })
			.IncludeChanges()
			.Subscribe(es =>
			{
				UnityEngine.Debug.Log("Testing SelectThese with ['foo', 'bar', null]: " + String.Join(", ", es.Select(e => e?.ID)));
			});
		this.Subs.Add(sub);

		sub = this.TestStore.Select(TestEntitiesConnector.SelectTheseTestEntities, new string[] { "foo", "bar", null })
			.IncludeChanges()
			.Select(l => l.Where(e => TestEntity.HasSome(e)))
			.Subscribe(es =>
			{
				UnityEngine.Debug.Log("Testing SelectTheseWhere Filtered with ['foo', 'bar', null]: " + String.Join(", ", es.Select(e => e?.ID)));
			});
		this.Subs.Add(sub);

		IObservable<string> filterObs = Observable.Return("Foo");
		var fitleredObs = TestEntitiesConnector.GetEntitiesFilteredBy(this.TestStore, filterObs, (entity, filter) => entity.Name.Contains(filter));
		sub = fitleredObs.Subscribe((es) =>
		{
			UnityEngine.Debug.Log("Testing Filtered to Include Foo in name: " + String.Join(", ", es.Select(e => e.Name)));
		});

		var uniRxPanel = DebugMenu.DebugMenuRoot.Instance.AddPanel("UniRx");
		uniRxPanel.AddButton("Test AfterFirstBehaviorSubject", () =>
		{
			var subs = new DisposableCollection();

			var first = 5;
			var second = 6;

			//Valid the AfterFirstBehaviorSubject
			var after = new AfterFirstBehaviorSubject<int>(7);
			var sub = after.Subscribe((v) =>
			{
				first = v;
			});
			subs.Add(sub);
			Debug.Log("AfterFirstBehaviorSubject First: " + first + " Second: " + second + " Value: " + after.Value);

			after.OnNext(8);
			Debug.Log("AfterFirstBehaviorSubject First: " + first + " Second: " + second + " Value: " + after.Value);

			sub = after.Subscribe((v) =>
			{
				second = v;
			});
			subs.Add(sub);
			Debug.Log("AfterFirstBehaviorSubject First: " + first + " Second: " + second + " Value: " + after.Value);

			after.OnNext(9);
			Debug.Log("AfterFirstBehaviorSubject First: " + first + " Second: " + second + " Value: " + after.Value);

			subs.Dispose();
		});

		LinkRoutingManager.Initialize("com.dropfake.oss.sandbox", UnityEngine.Debug.LogError, (bundle, logger) => new LinkRoutingManager(bundle, logger));

		LinkRoutingManager.Instance.AddHttpHandler((string url, Uri uri) =>
		{
			Application.OpenURL(url);
			return true;
		});

		LinkRoutingManager.Instance.AddLinkHandler("testing", (string url, Uri uri) =>
		{
			UnityEngine.Debug.Log("Url testing:" + url);
			return true;
		});

		LinkRoutingManager.Instance.AddLinkHandler("testing", (string url, Uri uri) =>
		{
			UnityEngine.Debug.Log("Url Override testing:" + url);
			return true;
		});

		var urlManagerPanel = DebugMenu.DebugMenuRoot.Instance.AddPanel("UrlManager");
		urlManagerPanel.AddButton("Internal Link", () =>
		{
			var interalUrl = "com.dropfake.oss.sandbox://testing/foo?bar=2";
			LinkRoutingManager.Instance.ProcessURL(interalUrl);
		});

		urlManagerPanel.AddButton("Internal Link As Http", () =>
		{
			var interalAsHttpUrl = "https://com.dropfake.oss.sandbox/testing/foo?bar=2";
			LinkRoutingManager.Instance.ProcessURL(interalAsHttpUrl);
		});

		urlManagerPanel.AddButton("Https", () =>
		{
			var httpsUrl = "https://example.com/blank.html#access_token=abcdefg&expires_in=86400&user_id=34558123";
			LinkRoutingManager.Instance.ProcessURL(httpsUrl);
		});

		urlManagerPanel.AddButton("Http", () =>
		{
			var httpUrl = "http://example.com/blank.html#access_token=abcdefg&expires_in=86400&user_id=34558123";
			LinkRoutingManager.Instance.ProcessURL(httpUrl);
		});

		var sampleNode = new SampleNode("foo");
		var sampleNodeActions = 0;
		WithChanges.SetAndOnChanged<ISampleNode>(sampleNode, (node) => { sampleNodeActions++; UnityEngine.Debug.Log("SampleNode actions: " + sampleNodeActions); });
		sampleNode.IsCompleteSubject.OnNext(true);

		DebugMenuRoot.Instance.Show = true;

		_scriptingBackend = new ScriptingBackend();
		_scriptingBackend.Init();
		_scriptingBackend.RunScript("main");

		var hello = "hello";
		var shuffled = hello.ShuffleString();
		Debug.Log($"Shuffled: {shuffled}");

		var webViewPanel = DebugMenu.DebugMenuRoot.Instance.AddPanel("WebView");
		webViewPanel.AddButton("Start WebView", () =>
		{
			this.StartWebView();
		});

		var cloudBuildPanel = DebugMenu.DebugMenuRoot.Instance.AddPanel("Cloud Build");
		cloudBuildPanel.AddLabelledValue("Build Number", this.BuildNumber);
		cloudBuildPanel.AddLabelledValue("Branch", this.Branch);
		cloudBuildPanel.AddLabelledValue("Commit", this.Commit);

#if UNITY_EDITOR
		UniRedux.Tools.ReduxInspector.Instance?.DisplayStore(TestStore);
#endif
	}

	private async Task FetchBuildInfo()
	{
		try
		{
			var info = await CloudBuildInfo.ParseFromResources();
			this.BuildInfo.OnNext(info);
		}
		catch (Exception ex)
		{
		}
	}

	void Update()
	{
		ExampleDebugMenu?.Update();

		_scriptingBackend.SimUpdate();
	}

	private void SetupReduxStore()
	{
		var hasReduxInspector = false;
#if UNITY_EDITOR
		hasReduxInspector = UniRedux.Tools.ReduxInspector.FindFirstInstance() != null;
#endif
		TestStore = new ReduxStore<TestState>(TestReducers.CreateReducers(), TestState.InitialState, hasReduxInspector);

#if UNITY_EDITOR
		var hasActionEmitter = UniRedux.Tools.ReduxActionEmitter.FindFirstInstance() != null;
		if (hasActionEmitter)
		{
			UniRedux.Tools.ReduxActionEmitter.Instance.RegisterStore("Test Store", TestStore);
			UniRedux.Tools.ReduxActionEmitter.Instance.DetectActionsInAssembly(Assembly.GetExecutingAssembly());
		}
#endif
	}

	void StartWebView()
	{
		try
		{
			webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
			webViewObject.Init(
				cb: (msg) =>
				{
					Debug.Log(string.Format("CallFromJS[{0}]", msg));
				},
				err: (msg) =>
				{
					Debug.Log(string.Format("CallOnError[{0}]", msg));
				},
				httpErr: (msg) =>
				{
					Debug.Log(string.Format("CallOnHttpError[{0}]", msg));
				},
				started: (msg) =>
				{
					Debug.Log(string.Format("CallOnStarted[{0}]", msg));
				},
				hooked: (msg) =>
				{
					Debug.Log(string.Format("CallOnHooked[{0}]", msg));
				},
				cookies: (msg) =>
				{
					Debug.Log(string.Format("CallOnCookies[{0}]", msg));
				},
				ld: (msg) =>
				{
					Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS
					// NOTE: the following js definition is required only for UIWebView; if
					// enabledWKWebView is true and runtime has WKWebView, Unity.call is defined
					// directly by the native plugin.
#if true
					var js = @"
                    if (!(window.webkit && window.webkit.messageHandlers)) {
                        window.Unity = {
                            call: function(msg) {
                                window.location = 'unity:' + msg;
                            }
                        };
                    }
                ";
#else
                // NOTE: depending on the situation, you might prefer this 'iframe' approach.
                // cf. https://github.com/gree/unity-webview/issues/189
                var js = @"
                    if (!(window.webkit && window.webkit.messageHandlers)) {
                        window.Unity = {
                            call: function(msg) {
                                var iframe = document.createElement('IFRAME');
                                iframe.setAttribute('src', 'unity:' + msg);
                                document.documentElement.appendChild(iframe);
                                iframe.parentNode.removeChild(iframe);
                                iframe = null;
                            }
                        };
                    }
                ";
#endif
#elif UNITY_WEBPLAYER || UNITY_WEBGL
                var js = @"
                    window.Unity = {
                        call:function(msg) {
                            parent.unityWebView.sendMessage('WebViewObject', msg);
                        }
                    };
                ";
#else
					var js = "";
#endif
					webViewObject.EvaluateJS(js + @"Unity.call('ua=' + navigator.userAgent)");
				}
				//transparent: false,
				//zoom: true,
				//ua: "custom user agent string",
				//radius: 0,  // rounded corner radius in pixel
				//// android
				//androidForceDarkMode: 0,  // 0: follow system setting, 1: force dark off, 2: force dark on
				//// ios
				//enableWKWebView: true,
				//wkContentMode: 0,  // 0: recommended, 1: mobile, 2: desktop
				//wkAllowsLinkPreview: true,
				//// editor
				//separated: false
				);
		}
		catch (Exception e)
		{
			Debug.LogException(e, this);
		}
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		webViewObject.bitmapRefreshCycle = 1;
#endif
		// cf. https://github.com/gree/unity-webview/pull/512
		// Added alertDialogEnabled flag to enable/disable alert/confirm/prompt dialogs. by KojiNakamaru · Pull Request #512 · gree/unity-webview
		//webViewObject.SetAlertDialogEnabled(false);

		// cf. https://github.com/gree/unity-webview/pull/728
		//webViewObject.SetCameraAccess(true);
		//webViewObject.SetMicrophoneAccess(true);

		// cf. https://github.com/gree/unity-webview/pull/550
		// introduced SetURLPattern(..., hookPattern). by KojiNakamaru · Pull Request #550 · gree/unity-webview
		//webViewObject.SetURLPattern("", "^https://.*youtube.com", "^https://.*google.com");

		// cf. https://github.com/gree/unity-webview/pull/570
		// Add BASIC authentication feature (Android and iOS with WKWebView only) by takeh1k0 · Pull Request #570 · gree/unity-webview
		//webViewObject.SetBasicAuthInfo("id", "password");

		//webViewObject.SetScrollbarsVisibility(true);

		webViewObject.SetMargins(5, 100, 5, Screen.height / 4);
		webViewObject.SetTextZoom(100);  // android only. cf. https://stackoverflow.com/questions/21647641/android-webview-set-font-size-system-default/47017410#47017410
		webViewObject.SetVisibility(true);

		var url = "https://www.google.com";
		webViewObject.LoadURL(url);
	}
}
